package com.media.appmedia.clicks;

import java.util.ArrayList;

import com.media.appmedia.activity.AdWebView;
import com.media.appmedia.entity.ImageAdBean;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

public class ViewPagerAdOncliecks implements OnClickListener {
	private ArrayList<ImageAdBean> mList;
	private ResultUrl resultUrl;

	public ViewPagerAdOncliecks(ArrayList<ImageAdBean> list, ResultUrl resultUrl) {
		mList = list;
		this.resultUrl = resultUrl;
	}

	@Override
	public void onClick(View view) {
		String url = (String) view.getTag();
		resultUrl.setUrl(url);
	}

	public interface ResultUrl {
		void setUrl(String url);
	}
}
