package com.media.appmedia.lock;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * 开启广播，启动activity
 * @author Rose
 *
 */
public class LockScreenReceiver extends BroadcastReceiver {
	private final static String TAG = "LockScreenReceiver";
	private KeyguardManager keyguardManager = null;
	private KeyguardManager.KeyguardLock keyguardLock = null;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if(intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)){
			Intent mIntent = new Intent(context, LockScreenActivity.class);
			mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
			keyguardLock = keyguardManager.newKeyguardLock("");
			keyguardLock.disableKeyguard(); 
			
			context.startActivity(mIntent);
		}
	}

}
