package com.media.appmedia.lock;

import com.media.appmedia.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
/**
 * 
 * 作者:	Rose
 * 业务名: 自定义布局
 * 功能说明: 实现解锁滑动按钮变化
 * 编写日期:	2014-12-25
 *
 */
public class SliderRelativeLayout extends RelativeLayout {
	private final static String TAG = "SliderRelativeLayout";
	private Context context;
	private Bitmap dragBitmap = null; // 拖拽图片
	private int locationX = 0; // bitmap初始绘图位置
	private ImageView heartView = null; // 主要是获取相对布局的高度
	private ImageView leftRingView = null;
	private ImageView rightRingView = null;
	private Handler handler = null; // 信息传递
	private static int BACK_DURATION = 10; // 回退动画时间间隔值 20ms
	private static float VE_HORIZONTAL = 0.9f; // 水平方向前进速率 0.1dip/ms

	public SliderRelativeLayout(Context context) {
		super(context);
		SliderRelativeLayout.this.context = context;
		intiDragBitmap();
	}

	public SliderRelativeLayout(Context context, AttributeSet attrs) {
		super(context, attrs, 0);
		SliderRelativeLayout.this.context = context;
		intiDragBitmap();
	}

	public SliderRelativeLayout(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		SliderRelativeLayout.this.context = context;
		intiDragBitmap();
	}

	/**
	 * 得到拖拽图片
	 */
	private void intiDragBitmap() {

		if (dragBitmap == null) {
			dragBitmap = BitmapFactory.decodeResource(context.getResources(),
					R.drawable.ic_lock_normal);
		}
		// 初始化为屏幕一半
		locationX = (getScreenWidth()) / 2;
	}

	/**
	 * 这个方法里可以得到一个其他资源
	 */
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		heartView = (ImageView) findViewById(R.id.loveView);
		leftRingView = (ImageView) findViewById(R.id.leftRing);
		rightRingView = (ImageView) findViewById(R.id.rightRing);
	}

	/**
	 * 对拖拽图片不同的点击事件处理
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int X = (int) event.getX();

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			locationX = (int) event.getX();
			if(isActionDown(event)){
				dragBitmap = BitmapFactory.decodeResource(context.getResources(),
						R.drawable.ic_lock_pressed);
				leftRingView.setImageResource(R.drawable.icon_left_f);
				rightRingView.setImageResource(R.drawable.icon_right_f);
			}
			return isActionDown(event);// 判断是否点击了滑动区域

		case MotionEvent.ACTION_MOVE: // 保存x轴方向，绘制图画
			locationX = X;
			invalidate(); // 重新绘图
			return true;

		case MotionEvent.ACTION_UP: // 判断是否解锁成功
			if (!isLocked(true)) { // 没有解锁成功,动画应该回退
				dragBitmap = BitmapFactory.decodeResource(context.getResources(),
						R.drawable.ic_lock_normal);
				handleActionUpEvent(event); // 动画回退
				leftRingView.setImageResource(R.drawable.icon_left_n);
				rightRingView.setImageResource(R.drawable.icon_right_n);
			}
			return true;
		}
		return super.onTouchEvent(event);
	}

	/**
	 * 回退动画
	 * 
	 * @param event
	 */
	private void handleActionUpEvent(MotionEvent event) {
		int x = (int) event.getX();
		// int toLeft = leftRingView.getWidth()/2;
		// if(locationX >= getScreenWidth()/2){
		// locationX = x - toLeft;
		//
		// }else if(locationX <getScreenWidth()/2){
		// locationX = x + toLeft;
		// }
		handler.postDelayed(ImageBack, BACK_DURATION); // 回退
	}

	/**
	 * 未解锁时，图片回退
	 */
	private Runnable ImageBack = new Runnable() {
		@Override
		public void run() {
			// 此处为当滑动的
			if (locationX >= getScreenWidth() / 2) {
				locationX = locationX - (int) (VE_HORIZONTAL * BACK_DURATION);
				if (locationX >= getScreenWidth() / 2) {
					handler.postDelayed(ImageBack, BACK_DURATION); // 回退
					invalidate();
				}
			} else if (locationX <= getScreenWidth() / 2) {
				locationX = locationX + (int) (VE_HORIZONTAL * BACK_DURATION);
				if (locationX < getScreenWidth() / 2) {
					handler.postDelayed(ImageBack, BACK_DURATION); // 回退
					invalidate();
				}
			}
		}
	};

	/**
	 * 判断是否点击到了滑动区域
	 * 
	 * @param event
	 * @return
	 */
	private boolean isActionDown(MotionEvent event) {
		Rect rect = new Rect();
		heartView.getHitRect(rect);
		boolean isIn = rect.contains((int) event.getX() - heartView.getWidth(),
				(int) event.getY());
		if (isIn) {
			heartView.setVisibility(View.GONE);
			return true;
		}
		return false;
	}

	/**
	 * 绘制拖动时的图片
	 */
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		invalidateDragImg(canvas);
	}

	/**
	 * 图片随手势移动
	 * 
	 * @param canvas
	 */
	private void invalidateDragImg(Canvas canvas) {
		int drawX = locationX - dragBitmap.getWidth() / 2;
		int drawY = heartView.getTop();

		if (drawX == getScreenWidth()) { // 划到最左边
			heartView.setVisibility(View.VISIBLE);
			return;
		} else {
			if (isLocked(false)) { // 判断是否成功
				return;
			}
			heartView.setVisibility(View.GONE);
			canvas.drawBitmap(dragBitmap, drawX < 0 ? 5 : drawX, drawY, null);

		}
	}

	/**
	 * 判断是否解锁
	 */
	private boolean isLocked(boolean flage) {
		if (locationX > (getScreenWidth() - rightRingView.getWidth())) {
			if(locationX + dragBitmap.getWidth()/2 > getScreenWidth() - rightRingView.getWidth()){
				rightRingView.setImageResource(R.drawable.icon_right_s);
			}else{
				rightRingView.setImageResource(R.drawable.icon_right_f);
			}
			if(flage){
				handler.obtainMessage(LockScreenActivity.MSG_LOCK_SUCESS_RIGHT)
				.sendToTarget();
			}
			return true;
		} else if (locationX <= leftRingView.getWidth()) {
			if(locationX - dragBitmap.getWidth()/2 <= leftRingView.getWidth()){
				leftRingView.setImageResource(R.drawable.icon_left_s);
			}else{
				leftRingView.setImageResource(R.drawable.icon_left_f);
			}
			if(flage){
				handler.obtainMessage(LockScreenActivity.MSG_LOCK_SUCESS_LEFT)
				.sendToTarget();
			}
			return true;
		}
		return false;
	}

	/**
	 * 获取屏幕宽度
	 * 
	 * @return
	 */
	private int getScreenWidth() {
		WindowManager manager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		int width = manager.getDefaultDisplay().getWidth();
		return width;
	}

	/**
	 * 与主activity通信
	 * 
	 * @param handler
	 */
	public void setMainHandler(Handler handler) {
		this.handler = handler;
	}

	/**
	 * 
	 * 作者: Rose 
	 * 业务名: 回调接口 
	 * 功能说明: 用于回调更改左右俩边的图片 
	 * 编写日期: 2014-12-26
	 * 
	 */
	public interface ChangeView {
		void putView(int type, ImageView view);
	}
	
}
