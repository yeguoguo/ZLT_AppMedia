package com.media.appmedia.lock;


import com.media.appmedia.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
/**
 * 
 * 作者:	Rose
 * 业务名:	用于展示广告链接内容
 * 功能说明: 
 * 编写日期:	2014-12-3
 *
 */
public class LockADWebView extends Activity {
	private WebView webView;
	private String url;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去掉标题栏
		setContentView(R.layout.lock_webview_layout);
		url = getIntent().getStringExtra("url");
		webView =(WebView) findViewById(R.id.web_view);
		if (url != null) {
			WebSettings webSetting = webView.getSettings();
			webSetting.setJavaScriptEnabled(true);
			webView.requestFocusFromTouch();
			webView.setWebViewClient(new WebViewClient() {
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					view.loadUrl(url);
					return true;
				}
			});
			webView.loadUrl(url);
		}
	}


//	@Override
//	// 设置回退
//	// 覆盖Activity类的onKeyDown(int keyCoder,KeyEvent event)方法
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
//			webView.goBack(); // goBack()表示返回WebView的上一页面
//			return true;
//		}
//		return false;
//	}
}
