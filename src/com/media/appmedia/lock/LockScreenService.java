package com.media.appmedia.lock;

import android.app.KeyguardManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

public class LockScreenService extends Service {
	private final static String TAG = "LockScreenService";
	private Intent lockIntent;
	private KeyguardManager keyguardManager = null;
	private KeyguardManager.KeyguardLock keyguardLock = null;

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		// 自启服务间隔自动刷新
		new Thread() {
			public void run() {
				while (true) {
					try {
						Thread.sleep(3000);
						Intent intent = new Intent(
								"com.appmedia.media.resgest.service");
						sendBroadcast(intent);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
		lockIntent = new Intent(LockScreenService.this,
				LockScreenActivity.class);
		lockIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		// 注册广播
		IntentFilter mScreenOffFilter = new IntentFilter(
				Intent.ACTION_SCREEN_OFF);
		LockScreenService.this.registerReceiver(mScreenOffReceiver,
				mScreenOffFilter);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return Service.START_STICKY;
	}

	/**
	 * 屏幕变亮的广播，这里要隐藏系统的锁屏界面
	 */
	private BroadcastReceiver mScreenOffReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.i(TAG, intent.getAction());
			if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)
					|| intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {

//				keyguardManager = (KeyguardManager) context
//						.getSystemService(Context.KEYGUARD_SERVICE);
//				keyguardLock = keyguardManager.newKeyguardLock("");
//				keyguardLock.disableKeyguard(); // 这里就是取消系统默认的锁屏

				startActivity(lockIntent); // 注意这里跳转的意图
			}
		}
	};
}
