package com.media.appmedia.setting;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.media.appmedia.R;
import com.media.appmedia.activity.AdWebView;
import com.media.appmedia.adapter.SystemMessgesAdapter;
import com.media.appmedia.entity.SystemMessgesBean;
import com.media.appmedia.network.ProtocolService;
import com.media.appmedia.util.MethodUtils;
import com.media.appmedia.util.PhoneInfoUtils;
import com.media.appmedia.util.ToastUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

/**
 * 
 * <pre>
 * 业务名:
 * 功能说明: 设置里面的意见反馈
 * 编写日期:	2014-11-13
 * 作者:	 季佳满
 * 
 * 历史记录
 * 1、修改日期：
 *    修改人：
 *    修改内容：
 * </pre>
 */
public class FanActivity extends Activity {
	@ViewInject(R.id.EditText00)
	private EditText textWall;
	@ViewInject(R.id.EditText01)
	private EditText textSoft;
	@ViewInject(R.id.EditText02)
	private EditText textInter;
	@ViewInject(R.id.EditText04)
	private EditText textContent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去掉标题栏
		setContentView(R.layout.opinion_layout);
		ViewUtils.inject(this);
	}

	@OnClick({ R.id.come_back })
	public void back(View v) {
		FanActivity.this.finish();
	}

	/**
	 * 
	 * 方法说明：提交触发事件
	 * 
	 * @param view
	 */
	@OnClick(R.id.fankiuanniu)
	public void comint(View view) {
		String wall = textWall.getText().toString().trim();
		if (MethodUtils.isEmpty(wall)) {
			ToastUtils.show(getApplicationContext(), "请输入金币途径");
			return;
		}
		String soft = textSoft.getText().toString().trim();
		if (MethodUtils.isEmpty(wall)) {
			ToastUtils.show(getApplicationContext(), "请输入任务名称");
			return;
		}
		String interge = textInter.getText().toString().trim();
		if (MethodUtils.isEmpty(wall)) {
			ToastUtils.show(getApplicationContext(), "请输入金币数量");
			return;
		}
		String content = textContent.getText().toString().trim();
		if (MethodUtils.isEmpty(wall)) {
			ToastUtils.show(getApplicationContext(), "请简单描述");
			return;
		}

		putFeedBack(PhoneInfoUtils.getIMEI(getApplicationContext()), wall,
				soft, interge, content);

	}

	private void putFeedBack(String biaoshifu, String wall, String soft,
			String integral, String content) {
		ProtocolService.putFeedBack(biaoshifu, wall, soft, integral, content,
				new RequestCallBack<String>() {
					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						JSONObject obj;
						try {
							obj = new JSONObject(arg0.result);
							int mess = obj.getInt("message");
							AlertDialog.Builder alertbBuilder = new AlertDialog.Builder(
									FanActivity.this);
							alertbBuilder.setTitle("提示").setPositiveButton(
									"确定",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											// 不做处理
											dialog.cancel();
											FanActivity.this.finish();
										}
									});
							if (mess == 1) {
								alertbBuilder.setMessage("提交成功！").create();
								alertbBuilder.show();
							} else {
								alertbBuilder.setMessage("提交失败！").create();
								alertbBuilder.show();
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}

					}

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						ToastUtils.show(getApplicationContext(),
								R.string.phone_verify_send_fail);
					}
				});
	}
}
