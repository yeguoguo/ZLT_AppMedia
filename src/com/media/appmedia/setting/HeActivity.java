package com.media.appmedia.setting;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.media.appmedia.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

/**
 * 
 * <pre>
 * 业务名:
 * 功能说明: 设置里的商业合作
 * 编写日期:	2014-11-13
 * 作者:	 季佳满
 * 
 * 历史记录
 * 1、修改日期：
 *    修改人：
 *    修改内容：
 * </pre>
 */
public class HeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去掉标题栏
		setContentView(R.layout.act_about);
		ViewUtils.inject(this);

	}

	@OnClick({ R.id.come_back})
	public void back(View v) {
		HeActivity.this.finish();
	}
}
