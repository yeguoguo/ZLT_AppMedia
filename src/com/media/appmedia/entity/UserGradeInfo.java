package com.media.appmedia.entity;

import java.io.Serializable;

public class UserGradeInfo implements Serializable {
	private int message;
	private float integral_grande;
	private String regular_name;
	
	
	public float getIntegral_grande() {
		return integral_grande;
	}
	public void setIntegral_grande(float integral_grande) {
		this.integral_grande = integral_grande;
	}
	public int getMessage() {
		return message;
	}
	public void setMessage(int message) {
		this.message = message;
	}
	public String getRegular_name() {
		return regular_name;
	}
	public void setRegular_name(String regular_name) {
		this.regular_name = regular_name;
	}
	@Override
	public String toString() {
		return "UserGradeInfo [message=" + message + ", integral_grande="
				+ integral_grande + ", regular_name=" + regular_name + "]";
	}
	
	
	
	
}
