package com.media.appmedia.entity;

import java.io.Serializable;
/**
 * 
 * <pre>
 * 业务名:
 * 功能说明: 手机验证码实体类
 * 
 * 编写日期:	2014-11-16
 * 作者:	 季佳满
 * 
 * 历史记录
 * 1、修改日期：
 *    修改人：
 *    修改内容：
 * </pre>
 */
public class BaseEntity implements Serializable {
	private static final long serialVersionUID = 5003999270755642366L;
	
	private String rand;
	private int error;
	private String message;
	
	public String getRand() {
		return rand;
	}
	public void setRand(String rand) {
		this.rand = rand;
	}
	public int getError() {
		return error;
	}
	public void setError(int error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isSuccess() {
		return error == 0;
	}
	public BaseEntity( String rand, int error, String message) {
		super();
		this.rand = rand;
		this.error = error;
		this.message = message;
	}
	public BaseEntity() {
		super();
	}
	@Override
	public String toString() {
		return "BaseEntity [rand=" + rand + ", error=" + error + ", message="
				+ message + "]";
	}
	
}
