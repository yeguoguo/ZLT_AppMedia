package com.media.appmedia.entity;

public class ImageOneBean {
	private String share_image_id;
	private String share_image_photo;
	private String share_image_url;
	public String getShare_image_id() {
		return share_image_id;
	}
	public void setShare_image_id(String share_image_id) {
		this.share_image_id = share_image_id;
	}
	public String getShare_image_photo() {
		return share_image_photo;
	}
	public void setShare_image_photo(String share_image_photo) {
		this.share_image_photo = share_image_photo;
	}
	public String getShare_image_url() {
		return share_image_url;
	}
	public void setShare_image_url(String share_image_url) {
		this.share_image_url = share_image_url;
	}
	
	
}
