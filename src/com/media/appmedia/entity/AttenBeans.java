package com.media.appmedia.entity;

public class AttenBeans {
	private String atten_list_logo;
	private String atten_list_name;
	private String atten_list_src;
	private String atten_list_jinbi;
	private String atten_list_url;
	private String atten_list_count;
	private String atten_list_type;
	private String atten_list_money;
	private int atten_list_id;
	private String atten_list_title;
	private String atten_list_content;
	private String atten_list_photo;
	
	
	public String getAtten_list_title() {
		return atten_list_title;
	}
	public void setAtten_list_title(String atten_list_title) {
		this.atten_list_title = atten_list_title;
	}
	public String getAtten_list_content() {
		return atten_list_content;
	}
	public void setAtten_list_content(String atten_list_content) {
		this.atten_list_content = atten_list_content;
	}
	public String getAtten_list_photo() {
		return atten_list_photo;
	}
	public void setAtten_list_photo(String atten_list_photo) {
		this.atten_list_photo = atten_list_photo;
	}
	public int getAtten_list_id() {
		return atten_list_id;
	}
	public void setAtten_list_id(int atten_list_id) {
		this.atten_list_id = atten_list_id;
	}
	public String getAtten_list_money() {
		return atten_list_money;
	}
	public void setAtten_list_money(String atten_list_money) {
		this.atten_list_money = atten_list_money;
	}
	public String getAtten_list_type() {
		return atten_list_type;
	}
	public void setAtten_list_type(String atten_list_type) {
		this.atten_list_type = atten_list_type;
	}
	public String getAtten_list_logo() {
		return atten_list_logo;
	}
	public void setAtten_list_logo(String atten_list_logo) {
		this.atten_list_logo = atten_list_logo;
	}
	public String getAtten_list_name() {
		return atten_list_name;
	}
	public void setAtten_list_name(String atten_list_name) {
		this.atten_list_name = atten_list_name;
	}
	public String getAtten_list_src() {
		return atten_list_src;
	}
	public void setAtten_list_src(String atten_list_src) {
		this.atten_list_src = atten_list_src;
	}
	public String getAtten_list_jinbi() {
		return atten_list_jinbi;
	}
	public void setAtten_list_jinbi(String atten_list_jinbi) {
		this.atten_list_jinbi = atten_list_jinbi;
	}
	public String getAtten_list_url() {
		return atten_list_url;
	}
	public void setAtten_list_url(String atten_list_url) {
		this.atten_list_url = atten_list_url;
	}
	public String getAtten_list_count() {
		return atten_list_count;
	}
	public void setAtten_list_count(String atten_list_count) {
		this.atten_list_count = atten_list_count;
	}
	
	
	
}
