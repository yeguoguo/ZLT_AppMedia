package com.media.appmedia.entity;
/**
 * 网络模块字段
 * @author haibo
 *
 */
public class NetBean {

	private String imageSign;//图片
	private String url;//一个地址
	private String time;//倒计时
	private String type;//1为广告，2 为APP
	private String shId;//商户ID 
	private String vip;//1为会员，2 为非会员
	private String routeId;//路由器ID
	private String userId;//用户ID
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getImageSign() {
		return imageSign;
	}
	public void setImageSign(String imageSign) {
		this.imageSign = imageSign;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getShId() {
		return shId;
	}
	public void setShId(String shId) {
		this.shId = shId;
	}
	public String getVip() {
		return vip;
	}
	public void setVip(String vip) {
		this.vip = vip;
	}
	public String getRouteId() {
		return routeId;
	}
	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}
}
