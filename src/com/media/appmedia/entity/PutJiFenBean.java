package com.media.appmedia.entity;

import java.io.Serializable;

public class PutJiFenBean {
	private String sonsume_inte_type;
	private String sonsume_inte_integral;
	private String sonsume_inte_time;
	public String getSonsume_inte_type() {
		return sonsume_inte_type;
	}
	public void setSonsume_inte_type(String sonsume_inte_type) {
		this.sonsume_inte_type = sonsume_inte_type;
	}
	public String getSonsume_inte_integral() {
		return sonsume_inte_integral;
	}
	public void setSonsume_inte_integral(String sonsume_inte_integral) {
		this.sonsume_inte_integral = sonsume_inte_integral;
	}
	public String getSonsume_inte_time() {
		return sonsume_inte_time;
	}
	public void setSonsume_inte_time(String sonsume_inte_time) {
		this.sonsume_inte_time = sonsume_inte_time;
	}
	
}
