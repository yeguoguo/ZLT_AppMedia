package com.media.appmedia.entity;

import com.media.appmedia.util.MethodUtils;

public class SystemMessgesBean {
	private String system_cont_detail;
	private String system_cont_time;
	public String getSystem_cont_detail() {
		return system_cont_detail;
	}
	public void setSystem_cont_detail(String system_cont_detail) {
		this.system_cont_detail = system_cont_detail;
	}
	public String getSystem_cont_time() {
		return system_cont_time;
	}
	public void setSystem_cont_time(String system_cont_time) {
		this.system_cont_time = MethodUtils.getTimers(system_cont_time);
	}
	
	
	
	
}
