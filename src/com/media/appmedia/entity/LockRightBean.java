package com.media.appmedia.entity;

import java.io.Serializable;

public class LockRightBean implements Serializable{
	private int lock_image_count;
	private String lock_image_rightjifen;
	public int getLock_image_count() {
		return lock_image_count;
	}
	public void setLock_image_count(int lock_image_count) {
		this.lock_image_count = lock_image_count;
	}
	public String getLock_image_rightjifen() {
		return lock_image_rightjifen;
	}
	public void setLock_image_rightjifen(String lock_image_rightjifen) {
		this.lock_image_rightjifen = lock_image_rightjifen;
	}
	
	
}
