package com.media.appmedia.entity;

import java.io.Serializable;

public class GetJiFenBean{
	private String get_inte_type;
	private String get_inte_integral;
	private String get_inte_time;
	public String getGet_inte_type() {
		return get_inte_type;
	}
	public void setGet_inte_type(String get_inte_type) {
		this.get_inte_type = get_inte_type;
	}
	public String getGet_inte_integral() {
		return get_inte_integral;
	}
	public void setGet_inte_integral(String get_inte_integral) {
		this.get_inte_integral = get_inte_integral;
	}
	public String getGet_inte_time() {
		return get_inte_time;
	}
	public void setGet_inte_time(String get_inte_time) {
		this.get_inte_time = get_inte_time;
	}
	
	
}
