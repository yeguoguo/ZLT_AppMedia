package com.media.appmedia.entity;

import java.io.Serializable;
/**
 * 
 * 作者:	Rose
 * 业务名:当前积分和历史积分数据原型
 * 功能说明: 用于解析当前积分和历史积分的数据
 * 编写日期:	2014-12-1
 *
 */
public class JSON implements Serializable{
	private String history_grande;
	private String integral_grande;
	public String getHistory_grande() {
		return history_grande;
	}
	public void setHistory_grande(String history_grande) {
		this.history_grande = history_grande;
	}
	public String getIntegral_grande() {
		return integral_grande;
	}
	public void setIntegral_grande(String integral_grande) {
		this.integral_grande = integral_grande;
	}
	
}
