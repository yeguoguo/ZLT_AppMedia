package com.media.appmedia.entity;

import com.media.appmedia.util.MethodUtils;

public class MyMessgesBean {
	private String my_cont_detail;
	private String my_cont_time;
	public String getMy_cont_detail() {
		return my_cont_detail;
	}
	public void setMy_cont_detail(String my_cont_detail) {
		this.my_cont_detail = my_cont_detail;
	}
	public String getMy_cont_time() {
		return my_cont_time;
	}
	public void setMy_cont_time(String my_cont_time) {
		this.my_cont_time = MethodUtils.getTimers(my_cont_time);
	}
	
	
}
