package com.media.appmedia.entity;

import java.io.Serializable;

public class LockADBean implements Serializable{
	private int id;
	private int lock_image_count;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	private String lock_image_photo;
	private String lock_image_url;
	private String lock_image_leftjifen;
	private String lock_image_id;
	
	
	
	public int getLock_image_count() {
		return lock_image_count;
	}
	public void setLock_image_count(int lock_image_count) {
		this.lock_image_count = lock_image_count;
	}
	public String getLock_image_id() {
		return lock_image_id;
	}
	public void setLock_image_id(String lock_image_id) {
		this.lock_image_id = lock_image_id;
	}
	public String getLock_image_photo() {
		return lock_image_photo;
	}
	public void setLock_image_photo(String lock_image_photo) {
		this.lock_image_photo = lock_image_photo;
	}
	public String getLock_image_url() {
		return lock_image_url;
	}
	public void setLock_image_url(String lock_image_url) {
		this.lock_image_url = lock_image_url;
	}
	public String getLock_image_leftjifen() {
		return lock_image_leftjifen;
	}
	public void setLock_image_leftjifen(String lock_image_leftjifen) {
		this.lock_image_leftjifen = lock_image_leftjifen;
	}
	
	
	
}
