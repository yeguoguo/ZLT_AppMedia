package com.media.appmedia.entity;

import java.io.Serializable;

public class UserBean implements Serializable {
	private int message;
	private String role;
	private String history;
	private String share;
	
	public String getShare() {
		return share;
	}
	public void setShare(String share) {
		this.share = share;
	}
	public int getMessage() {
		return message;
	}
	public void setMessage(int message) {
		this.message = message;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getHistory() {
		return history;
	}
	public void setHistory(String history) {
		this.history = history;
	}
	@Override
	public String toString() {
		return "UserBean [message=" + message + ", role=" + role + ", history="
				+ history + "]";
	}
	
	
	
	
}
