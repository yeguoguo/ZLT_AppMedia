package com.media.appmedia.entity;

import java.io.Serializable;

public class IsLoginBean implements Serializable{
	private String role;
	private String telephone;
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	
	
}
