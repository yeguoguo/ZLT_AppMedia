package com.media.appmedia.entity;

public class ImageAdBean {
	private String roll_image_url;
	private String roll_image_photo;
	public String getRoll_image_url() {
		return roll_image_url;
	}
	public void setRoll_image_url(String roll_image_url) {
		this.roll_image_url = roll_image_url;
	}
	public String getRoll_image_photo() {
		return roll_image_photo;
	}
	public void setRoll_image_photo(String roll_image_photo) {
		this.roll_image_photo = roll_image_photo;
	}
	
	
}
