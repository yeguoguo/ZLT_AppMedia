package com.media.appmedia.entity;


public class Registerben extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private String cellphone;
	private String verfiycode;
	private String userMac;
	private String shId;
	private String source;
	private String deviceType;
	private String one_shang;
	private String userCode;

	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getCellphone() {
		return cellphone;
	}
	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}
	public String getVerfiycode() {
		return verfiycode;
	}
	public void setVerfiycode(String verfiycode) {
		this.verfiycode = verfiycode;
	}
	public String getUserMac() {
		return userMac;
	}
	public void setUserMac(String userMac) {
		this.userMac = userMac;
	}
	public String getShId() {
		return shId;
	}
	public void setShId(String shId) {
		this.shId = shId;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getOne_shang() {
		return one_shang;
	}
	public void setOne_shang(String one_shang) {
		this.one_shang = one_shang;
	}
	public Registerben(String cellphone, String verfiycode, String userMac,
			String shId, String source, String deviceType, String one_shang) {
		super();
		this.cellphone = cellphone;
		this.verfiycode = verfiycode;
		this.userMac = userMac;
		this.shId = shId;
		this.source = source;
		this.deviceType = deviceType;
		this.one_shang = one_shang;
	}
	public Registerben() {
		super();
	}
	@Override
	public String toString() {
		return "Registerben [cellphone=" + cellphone + ", verfiycode="
				+ verfiycode + ", userMac=" + userMac + ", shId=" + shId
				+ ", source=" + source + ", deviceType=" + deviceType
				+ ", one_shang=" + one_shang + "]";
	}
	
	
}
