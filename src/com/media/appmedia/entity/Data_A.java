package com.media.appmedia.entity;

public class Data_A {

	private String history_integral;
	private String integral_grande;
	public String getHistory_integral() {
		return history_integral;
	}
	public void setHistory_integral(String history_integral) {
		this.history_integral = history_integral;
	}
	public String getIntegral_grande() {
		return integral_grande;
	}
	public void setIntegral_grande(String integral_grande) {
		this.integral_grande = integral_grande;
	}
	@Override
	public String toString() {
		return "Data_A [history_integral=" + history_integral
				+ ", integral_grande=" + integral_grande + "]";
	}
	public Data_A(String history_integral, String integral_grande) {
		super();
		this.history_integral = history_integral;
		this.integral_grande = integral_grande;
	}
	
}
