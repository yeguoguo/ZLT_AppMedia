package com.media.appmedia.entity;

import java.io.Serializable;

public class IsSignBean implements Serializable{
	private int message;
	private int sign_record;
	public int getMessage() {
		return message;
	}
	public void setMessage(int message) {
		this.message = message;
	}
	public int getSign_record() {
		return sign_record;
	}
	public void setSign_record(int sign_record) {
		this.sign_record = sign_record;
	}
	@Override
	public String toString() {
		return "IsSignBean [message=" + message + ", sign_record="
				+ sign_record + "]";
	}
	
	
	
}
