package com.media.appmedia.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.media.appmedia.R;
import com.media.appmedia.adapter.SystemMessgesAdapter;
import com.media.appmedia.entity.SystemMessgesBean;
import com.media.appmedia.network.ProtocolService;
import com.media.appmedia.util.PhoneInfoUtils;
import com.media.appmedia.util.ToastUtils;

public class MessgSystemFragment extends Fragment {
	@ViewInject(R.id.system_messges_list)
	private ListView myMessList;
	@ViewInject(R.id.hintsys)
	private TextView hint1;
	private ArrayList<SystemMessgesBean> list = new ArrayList<SystemMessgesBean>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.system_message_fragment, null);
		ViewUtils.inject(this, mView);
		getSystemMessges(PhoneInfoUtils.getIMEI(getActivity()));
		myMessList.setEmptyView(hint1);
		return mView;
	}

	private void getSystemMessges(String biaoshifu) {
		ProtocolService.getSystemMessges(biaoshifu,
				new RequestCallBack<String>() {
					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							org.json.JSONObject obj = new org.json.JSONObject(
									arg0.result);
							if (obj.getInt("message") == 1) {
								JSONArray array = new JSONArray(obj
										.getString("0"));
								for (int i = 0; i < array.length(); i++) {
									org.json.JSONObject Obj = array
											.getJSONObject(i);
									SystemMessgesBean bean = new SystemMessgesBean();
									bean.setSystem_cont_detail(Obj
											.getString("system_cont_detail"));
									bean.setSystem_cont_time(Obj
											.getString("system_cont_time"));
									list.add(bean);
								}
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
						SystemMessgesAdapter adapter = new SystemMessgesAdapter(
								list, getActivity());
						myMessList.setAdapter(adapter);
					}

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						ToastUtils.show(getActivity(),
								R.string.phone_verify_send_fail);
					}
				});
	}
}
