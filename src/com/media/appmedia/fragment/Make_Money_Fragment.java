package com.media.appmedia.fragment;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.media.appmedia.R;
import com.media.appmedia.contarl.FragmentTabAdapter;
import com.media.appmedia.contarl.FragmentTabAdapter.OnRgsExtraCheckedChangedListener;

/**
 * 
 * <pre>
 * 业务名:
 * 功能说明:  Fragment赚钱功能
 * 编写日期:	2014-10-22
 * 作者:	 Rose
 * 
 * 历史记录
 * 1、修改日期：
 *    修改人：
 *    修改内容：
 * </pre>
 */
public class Make_Money_Fragment extends Fragment {

	private View mView;
	@ViewInject(R.id.radiogroup)
	private RadioGroup radioGroup;
	private ArrayList<Fragment> listFragments;
	@ViewInject(R.id.linear_tab)
	private LinearLayout linearLayout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.makemoneyfragment, null);
		ViewUtils.inject(this, mView);
		listFragments = new ArrayList<Fragment>();
		listFragments.add(new Make_Money_Childer1());
		listFragments.add(new Make_Money_Childer2());
		listFragments.add(new Make_Money_Childer3());
		FragmentTabAdapter fragmentTabAdapter = new FragmentTabAdapter(
				getFragmentManager(), listFragments, R.id.make_layout,
				radioGroup);
		fragmentTabAdapter.setOnRgsExtraCheckedChangedListener(new OnRgsExtraCheckedChangedListener() {
			
			@Override
			public void OnRgsExtraCheckedChanged(RadioGroup radioGroup, int checkedId,
					int index) {
				for(int i = 0;i<3;i++){
					TextView tv = (TextView) linearLayout.getChildAt(i);
					tv.setVisibility(View.VISIBLE);
				}
				if(index == 0){
					TextView tvs = (TextView) linearLayout.getChildAt(0);
					tvs.setVisibility(View.INVISIBLE);
				}else if(index == 1){
					TextView tvs = (TextView) linearLayout.getChildAt(0);
					tvs.setVisibility(View.INVISIBLE);
					tvs = (TextView) linearLayout.getChildAt(1);
					tvs.setVisibility(View.INVISIBLE);
				}else{
					TextView tvs = (TextView) linearLayout.getChildAt(1);
					tvs.setVisibility(View.INVISIBLE);
					
				}
			}
		});
		return mView;
	}

}
