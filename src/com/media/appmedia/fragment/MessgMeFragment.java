package com.media.appmedia.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.media.appmedia.R;
import com.media.appmedia.adapter.MyMessgesAdapter;
import com.media.appmedia.entity.MyMessgesBean;
import com.media.appmedia.network.ProtocolService;
import com.media.appmedia.util.PhoneInfoUtils;
import com.media.appmedia.util.ToastUtils;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class MessgMeFragment extends Fragment {
	@ViewInject(R.id.my_messges_list)
	private ListView myMessList;
	@ViewInject(R.id.hintme)
	private TextView hint;
	private ArrayList<MyMessgesBean> list = new ArrayList<MyMessgesBean>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.me_message_fragment, null);
		ViewUtils.inject(this, mView);
		getMyMessges(PhoneInfoUtils.getIMEI(getActivity()));
		myMessList.setEmptyView(hint);
		return mView;
	}

	private void getMyMessges(String biaoshifu) {
		ProtocolService.getMyMessges(biaoshifu, new RequestCallBack<String>() {
			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				try {
					org.json.JSONObject obj = new org.json.JSONObject(
							arg0.result);
					if (obj.getInt("message") == 1) {
						JSONArray array = new JSONArray(obj.getString("0"));
						for (int i = 0; i < array.length(); i++) {
							org.json.JSONObject Obj = array.getJSONObject(i);
							MyMessgesBean bean = new MyMessgesBean();
							bean.setMy_cont_detail(Obj
									.getString("my_cont_detail"));
							bean.setMy_cont_time(Obj.getString("my_cont_time"));
							list.add(bean);
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				MyMessgesAdapter adapter = new MyMessgesAdapter(list,
						getActivity());
				myMessList.setAdapter(adapter);
			}

			@Override
			public void onFailure(HttpException arg0, String arg1) {
				ToastUtils.show(getActivity(), R.string.phone_verify_send_fail);
			}
		});
	}
}
