package com.media.appmedia.fragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.media.appmedia.R;
import com.media.appmedia.entity.ImageOneBean;
import com.media.appmedia.exchange.AlipayActivity;
import com.media.appmedia.exchange.CostActivity;
import com.media.appmedia.exchange.GiftActivity;
import com.media.appmedia.exchange.QQActivity;
import com.media.appmedia.exchange.TenpayActivity;
import com.media.appmedia.exchange.YinhangkaActivity;
import com.media.appmedia.network.ProtocolService;
import com.media.appmedia.util.PhoneInfoUtils;
import com.media.appmedia.util.ToastUtils;

/**
 * 
 * <pre>
 * 业务名:
 * 功能说明:  Fragment 兑换功能
 * 编写日期:	2014-10-22
 * 作者:	 季佳满
 * 
 * 历史记录
 * 1、修改日期：
 *    修改人：
 *    修改内容：
 * </pre>
 */
public class Exchange_Fragment extends Fragment implements OnClickListener {

	private View mView;
	private RelativeLayout mRelativeLayoutQQ;
	private RelativeLayout mRelativeLayoutgift;
	private RelativeLayout mRelativeLayoutcost;
	private RelativeLayout mRelativeLayoutextract;
	private RelativeLayout mRelativeLayoutalipay;
	private RelativeLayout mRelativeLayouyinhang;
	private ImageOneBean bean;
	@ViewInject(R.id.top_image)
	private ImageView topImage;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.exchangefragment, container, false);
		ViewUtils.inject(this, mView);
//		setImageView(PhoneInfoUtils.getIMEI(getActivity()));
		mView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

			}
		});
		initView();
		return mView;
	}

	private void initView() {
		mRelativeLayoutQQ = (RelativeLayout) mView
				.findViewById(R.id.RelativeLayoutQQ);
		mRelativeLayoutcost = (RelativeLayout) mView
				.findViewById(R.id.RelativeLayoutcost);
		mRelativeLayoutalipay = (RelativeLayout) mView
				.findViewById(R.id.RelativeLayoutalipay);
		mRelativeLayoutgift = (RelativeLayout) mView
				.findViewById(R.id.RelativeLayoutgift);
		mRelativeLayoutextract = (RelativeLayout) mView
				.findViewById(R.id.RelativeLayoutextract);
		mRelativeLayouyinhang = (RelativeLayout) mView
				.findViewById(R.id.RelativeLayouyinhang);

		mRelativeLayoutQQ.setOnClickListener(this);
		mRelativeLayoutcost.setOnClickListener(this);
		mRelativeLayoutalipay.setOnClickListener(this);
		mRelativeLayoutgift.setOnClickListener(this);
		mRelativeLayoutextract.setOnClickListener(this);
		mRelativeLayouyinhang.setOnClickListener(this);

	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.RelativeLayoutQQ:
			Intent intentqq = new Intent(getActivity(), QQActivity.class);
			startActivity(intentqq);
			break;

		case R.id.RelativeLayoutcost:
			Intent intentCost = new Intent(getActivity(), CostActivity.class);
			startActivity(intentCost);
			break;
		case R.id.RelativeLayoutalipay:
			Intent intentAlipay = new Intent(getActivity(),
					AlipayActivity.class);
			startActivity(intentAlipay);
			break;
		case R.id.RelativeLayoutextract:
			Intent intentTenpay = new Intent(getActivity(),
					TenpayActivity.class);
			startActivity(intentTenpay);
			break;
		case R.id.RelativeLayoutgift:
			Intent intentGift = new Intent(getActivity(), GiftActivity.class);
			startActivity(intentGift);
			break;
		case R.id.RelativeLayouyinhang:
			Intent intentYinhang = new Intent(getActivity(), YinhangkaActivity.class);
			startActivity(intentYinhang);
			break;
		}
	}
//
//	private void setImageView(String biaoshifu) {
//		ProtocolService.getImageView(biaoshifu, new RequestCallBack<String>() {
//			@Override
//			public void onSuccess(ResponseInfo<String> arg0) {
//				try {
//					org.json.JSONObject obj = new org.json.JSONObject(
//							arg0.result);
//					if (obj.getInt("message") == 1) {
//						System.out.println("");
//						JSONArray array = obj.getJSONArray("0");
//						JSONObject Obj = array.getJSONObject(0);
//						bean = new ImageOneBean();
//						bean.setShare_image_id(Obj.getString("share_image_id"));
//						bean.setShare_image_photo(Obj.getString("share_image_photo"));
//						bean.setShare_image_url(Obj
//								.getString("share_image_url"));
//						String url = bean.getShare_image_photo();
//						// Xutils加载网络图片
//						BitmapUtils bitmapUtils = new BitmapUtils(getActivity());
//						// 加载网络图片
//						bitmapUtils.display(topImage, url);
//					}
//				} catch (JSONException e) {
//					e.printStackTrace();
//				}
//			}
//
//			@Override
//			public void onFailure(HttpException arg0, String arg1) {
//				ToastUtils.show(getActivity(), R.string.phone_verify_send_fail);
//			}
//		});
//	}

}
