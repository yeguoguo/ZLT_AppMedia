package com.media.appmedia.fragment;

import java.util.Set;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.media.appmedia.R;
import com.media.appmedia.appupdata.CurrentVersion;
import com.media.appmedia.appupdata.NewVersion;
import com.media.appmedia.base.BaseApplication;
import com.media.appmedia.lock.LockScreenService;
import com.media.appmedia.network.ProtocolConst;
import com.media.appmedia.setting.FanActivity;
import com.media.appmedia.setting.HeActivity;
import com.media.appmedia.setting.ZaiActivity;
import com.media.appmedia.util.MethodUtils;
import com.media.appmedia.util.ToastUtils;
import com.media.appmedia.view.SwitchButton;
import com.media.appmedia.view.SwitchButton.OnChangeListener;

/**
 * 
 * <pre>
 * 业务名:
 * 功能说明:  Fragment设置功能
 * 编写日期:	2014-12-02
 * 作者:	 Rose
 * 
 * 历史记录
 * 1、修改日期：
 *    修改人：
 *    修改内容：
 * </pre>
 */
public class Setting_Fragment extends Fragment implements OnClickListener,
	OnChangeListener {
	private String downloadPath = ProtocolConst.VERSION_DOWLOAC;
	@ViewInject(R.id.ban_text)
	private TextView banbenTextView;
	private LinearLayout mLinearLayoutzaixian;
	private LinearLayout mLinearLayoutfankui;
	private LinearLayout mLinearLayouthezuo;
	private LinearLayout mLinearLayoutbanben;
	private SwitchButton mLock_Screen_WiperSwitch, mNotice_Board_switch, mSound;
	private View mView;
	private BaseApplication application;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.settingfragment, container, false);
		initView();
		ViewUtils.inject(this, mView);
		// 设置版本展示
		setBanbenView();
		return mView;
	}

	private void initView() {
		mLock_Screen_WiperSwitch = (SwitchButton) mView
				.findViewById(R.id.vibrator_switch);
		mNotice_Board_switch = (SwitchButton) mView
				.findViewById(R.id.Notice_Board_switch);
		mSound =  (SwitchButton) mView.findViewById(R.id.Sound_switch);
		mLinearLayoutzaixian = (LinearLayout) mView.findViewById(R.id.zaixian);
		mLinearLayoutfankui = (LinearLayout) mView.findViewById(R.id.fankui);
		mLinearLayouthezuo = (LinearLayout) mView.findViewById(R.id.hezuo);
		mLinearLayoutbanben = (LinearLayout) mView.findViewById(R.id.banben);
		mLock_Screen_WiperSwitch.setOnChangeListener(this);
		mNotice_Board_switch.setOnChangeListener(this);
		mSound.setOnChangeListener(this);
		mLinearLayoutbanben.setOnClickListener(this);
		mLinearLayoutzaixian.setOnClickListener(this);
		mLinearLayoutfankui.setOnClickListener(this);
		mLinearLayouthezuo.setOnClickListener(this);
		application = BaseApplication.getApplication();
		mLock_Screen_WiperSwitch.setChecked(application.isSetLock());
		mNotice_Board_switch.setChecked(application.isSetNotfind());
		mSound.setChecked(application.isSound());
	}

	/**
	 * 
	 * 方法说明：获得并设置当前apk的版本号
	 * 
	 */
	private void setBanbenView() {
		String visier = CurrentVersion.getVersinName(getActivity());
		if (!MethodUtils.isEmpty(visier)) {
			banbenTextView.setText("v" + visier);
		}
	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.banben:
			// 放入一键调用版本检测并更新的方法
			getVersion(getActivity());
			break;
		case R.id.zaixian:
			Intent intentzai = new Intent(getActivity(), ZaiActivity.class);
			startActivity(intentzai);
			break;
		case R.id.fankui:
			Intent intentfan = new Intent(getActivity(), FanActivity.class);
			startActivity(intentfan);
			break;
		case R.id.hezuo:
			Intent intenthe = new Intent(getActivity(), HeActivity.class);
			startActivity(intenthe);
			break;
		}

	}

	/**
	 * 监听并存储当前用户设置的状态
	 * 
	 */
	@Override
	public void onChange(SwitchButton sb, boolean state) {
		switch (sb.getId()) {
		case R.id.vibrator_switch:
			application.setSetLock(state);
			MethodUtils.setSettingInfo1(application, state);
			if(state){
				BaseApplication.getApplication().startLockService();
			}else{
				BaseApplication.getApplication().stopLockService();
			}
			break;
		case R.id.Notice_Board_switch:
			application.setSetNotfind(state);
			MethodUtils.setSettingInfo2(application, state);
			if(state == true){
				ToastUtils.show(getActivity(), "通知栏通知提醒已开启");
				JPushInterface.setAlias(getActivity(), "sgfc88", new TagAliasCallback() {
					
					@Override
					public void gotResult(int arg0, String arg1, Set<String> arg2) {
					}
				});
			}else{
				ToastUtils.show(getActivity(), "通知栏通知提醒已关闭");
				JPushInterface.setAlias(getActivity(), "", new TagAliasCallback() {
					
					@Override
					public void gotResult(int arg0, String arg1, Set<String> arg2) {
					}
				});
			}
			break;
		case R.id.Sound_switch:
			application.setSound(state);
			MethodUtils.setSettingInfo3(application, state);
			break;
		}
	}
	
	
	
	/**
	 * 版本更新一键调用,解决一切后顾之忧
	 */
	public void getVersion(Context context) {
		try {
			new NewVersion(context, downloadPath, null)
					.checkUpdateVersion();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	

}
