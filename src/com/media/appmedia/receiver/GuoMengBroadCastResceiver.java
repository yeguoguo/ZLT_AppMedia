//package com.media.appmedia.receiver;
//
//import java.util.ArrayList;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import com.lidroid.xutils.exception.HttpException;
//import com.lidroid.xutils.http.ResponseInfo;
//import com.lidroid.xutils.http.callback.RequestCallBack;
//import com.media.appmedia.R;
//import com.media.appmedia.entity.UserGradeInfo;
//import com.media.appmedia.network.ProtocolService;
//import com.media.appmedia.util.PhoneInfoUtils;
//import com.media.appmedia.util.ToastUtils;
//
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//
///**
// * 
// * 作者: Rose 业务名: 功能说明: 用于接口回调，对果盟任务奖励的广播接受 编写日期: 2014-12-8
// * 
// */
//public class GuoMengBroadCastResceiver extends BroadcastReceiver {
//	private ArrayList<Integer> list = new ArrayList<Integer>();
//
//	@Override
//	public void onReceive(Context context, Intent intent) {
//		String result = intent.getExtras().getString("content");
//		try {
//			JSONObject obj = new JSONObject(result);
//			JSONObject OBJ = obj.getJSONObject("GUOMOB");
//			int flag = OBJ.getInt("Flag");
//			if (flag == 1) {
//				JSONArray array = OBJ.getJSONArray("Pay");
//				for (int i = 0; i < array.length(); i++) {
//					JSONObject objs = (JSONObject) array.get(i);
//					int gold = objs.getInt("Number");
//					list.add(gold);
//				}
//			}
//			for (int i = 0; i < list.size(); i++) {
//				setGold("果盟", "下载任务", list.get(i), context);
//			}
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * 
//	 * 方法说明：上传服务器数据(获得金币)
//	 * 
//	 * @param name
//	 * @param content
//	 * @param numb
//	 */
//	private void setGold(String name, String content, int numb,
//			final Context context) {
//		ProtocolService.setGolds(PhoneInfoUtils.getIMEI(context), name,
//				content, numb, new RequestCallBack<String>() {
//					@Override
//					public void onSuccess(ResponseInfo<String> arg0) {
//						System.out.println();
//					}
//
//					@Override
//					public void onFailure(HttpException arg0, String arg1) {
//						ToastUtils.show(context,
//								R.string.phone_verify_send_fail);
//					}
//				});
//	}
//
//}
