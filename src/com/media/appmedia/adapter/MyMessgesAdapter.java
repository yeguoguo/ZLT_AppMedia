package com.media.appmedia.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.media.appmedia.R;
import com.media.appmedia.base.MeBaseAdapter;
import com.media.appmedia.entity.MyMessgesBean;

public class MyMessgesAdapter extends MeBaseAdapter<MyMessgesBean> {

	public MyMessgesAdapter(List<MyMessgesBean> list, Context context) {
		super(list, context);
	}

	@Override
	public View createView(int position, View convertView, ViewGroup parent) {
		HoldView hold;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.messges_item, null);
			hold = new HoldView();
			ViewUtils.inject(hold, convertView);
			convertView.setTag(hold);
		} else {
			hold = (HoldView) convertView.getTag();
		}
		
		hold.messgesType.setText("我的消息");
		hold.messgesContent.setText(list.get(position).getMy_cont_detail());
		hold.messgesTime.setText(list.get(position).getMy_cont_time());
		return convertView;
	}

	static class HoldView {
		@ViewInject(R.id.messges_title)
		TextView messgesContent;
		@ViewInject(R.id.message_time)
		TextView messgesTime;
		@ViewInject(R.id.messges_type)
		TextView messgesType;

	}
}
