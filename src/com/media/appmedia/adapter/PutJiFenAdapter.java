package com.media.appmedia.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.media.appmedia.R;
import com.media.appmedia.base.MeBaseAdapter;
import com.media.appmedia.entity.PutJiFenBean;

/**
 * 
 * 作者: Rose 业务名: 功能说明: 消费的积分详情记录的适配器 编写日期: 2014-12-9
 * 
 */
public class PutJiFenAdapter extends MeBaseAdapter<PutJiFenBean> {

	public PutJiFenAdapter(List<PutJiFenBean> list, Context context) {
		super(list, context);
	}

	@Override
	public View createView(int position, View convertView, ViewGroup parent) {
		HoldView hold;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.integral_item_1, null);
			hold = new HoldView();
			ViewUtils.inject(hold, convertView);
			convertView.setTag(hold);
		} else {
			hold = (HoldView) convertView.getTag();
		}
		if(position%2 == 0){
			hold.layout.setBackgroundResource(R.drawable.huisebg);
		}else{
			hold.layout.setBackgroundResource(R.color.white);
		}
		hold.textTime.setText(list.get(position).getSonsume_inte_time());
		hold.textName.setText(list.get(position).getSonsume_inte_type());
		hold.textNumble.setText(list.get(position).getSonsume_inte_integral());
		return convertView;
	}

	static class HoldView {
		@ViewInject(R.id.biaoti)
		TextView textName;
		@ViewInject(R.id.jifen)
		TextView textNumble;
		@ViewInject(R.id.timer)
		TextView textTime;
		@ViewInject(R.id.ji2)
		LinearLayout layout;
	}

}
