package com.media.appmedia.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.media.appmedia.R;
import com.media.appmedia.base.MeBaseAdapter;
import com.media.appmedia.entity.AttenBeans;

public class AttenListAdapter extends MeBaseAdapter<AttenBeans> {

	public AttenListAdapter(List<AttenBeans> list, Context context) {
		super(list, context);
	}

	@Override
	public View createView(int position, View convertView, ViewGroup parent) {
		ViewHold hold = new ViewHold();
		if(convertView == null ){
			convertView = inflater.inflate(R.layout.atten_list_item, null);
			ViewUtils.inject(hold, convertView);
			convertView.setTag(hold);
		}else{
			hold = (ViewHold) convertView.getTag();
		}
		hold.textName.setText(list.get(position).getAtten_list_name());
		hold.textScr.setText(list.get(position).getAtten_list_src());
		BitmapUtils utils = new BitmapUtils(context);
		utils.display(hold.imageLog, list.get(position).getAtten_list_logo());
		hold.textGold.setText("奖励  "+list.get(position).getAtten_list_money()+"金币");
		return convertView;
	}
	
	public static class ViewHold{
		@ViewInject(R.id.text_name)
		TextView textName;
		@ViewInject(R.id.text_src)
		TextView textScr;
		@ViewInject(R.id.item_log)
		ImageView imageLog;
		@ViewInject(R.id.item_gold)
		TextView textGold;
	}

}
