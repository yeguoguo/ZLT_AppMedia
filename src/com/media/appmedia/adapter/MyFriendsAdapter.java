package com.media.appmedia.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.media.appmedia.R;
import com.media.appmedia.base.MeBaseAdapter;
import com.media.appmedia.entity.MyFriends;

public class MyFriendsAdapter extends MeBaseAdapter<MyFriends> {
	private int[] imageId = new int[] { R.drawable.xinxin1, R.drawable.xinxin2,
			R.drawable.xinxin3, R.drawable.xinxin4, R.drawable.xinxin5, };

	public MyFriendsAdapter(List<MyFriends> list, Context context) {
		super(list, context);
	}

	@Override
	public View createView(int position, View convertView, ViewGroup parent) {
		HoldView holdView;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.invitation_item, null);
			holdView = new HoldView();
			ViewUtils.inject(holdView, convertView);
			convertView.setTag(holdView);
		} else {
			holdView = (HoldView) convertView.getTag();
		}

		holdView.friendName.setText(list.get(position).getFriend_name());
		holdView.friendGrade.setText(list.get(position)
				.getGet_integral_to_fiend() + " 金");
		int id = getImageId(Float.parseFloat(list.get(position)
				.getGet_integral_to_fiend()));
		holdView.xinxinImage.setImageResource(imageId[id]);
		return convertView;
	}

	static class HoldView {
		@ViewInject(R.id.friend_name)
		TextView friendName;
		@ViewInject(R.id.friend_grade)
		TextView friendGrade;
		@ViewInject(R.id.friend_xinxin)
		ImageView xinxinImage;
	}

	private int getImageId(float m) {
		int n = 0;
		if (m <= 100) {
			n = 4;
		} else if (m <= 300) {
			n = 3;
		} else if (m <= 500) {
			n = 2;
		} else if (m <= 800) {
			n = 1;
		} else if (m > 800) {
			n = 0;
		}
		return n;
	}

}
