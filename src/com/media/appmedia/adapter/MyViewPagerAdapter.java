package com.media.appmedia.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * 
 * 作者: Rose 业务名: 功能说明: 用于适配ViewPager广告图片展示 编写日期: 2014-12-5
 * 
 */
public class MyViewPagerAdapter extends PagerAdapter {
	private ArrayList<ImageView> list;
	private Context mContext;

	public MyViewPagerAdapter(Context context, ArrayList<ImageView> list) {
		this.mContext = context;
		this.list = list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public boolean isViewFromObject(View obj0, Object obj1) {
		return obj0 == obj1;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {

		return super.instantiateItem(container, position);
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView(list.get(position));
		super.destroyItem(container, position, object);
	}

}
