package com.media.appmedia.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.media.appmedia.R;
import com.media.appmedia.base.MeBaseAdapter;

public class WallAdapter extends MeBaseAdapter<String> {

	public WallAdapter(List<String> list, Context context) {
		super(list, context);
	}

	@Override
	public View createView(int position, View convertView, ViewGroup parent) {
		HoldView holdView;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.makemoneyfragmen_itemt,
					null);
			holdView = new HoldView();
			holdView.wallName = (TextView) convertView
					.findViewById(R.id.textViewwanp);
			holdView.imageView = (ImageView) convertView.findViewById(R.id.imageViewqiandai);
			convertView.setTag(holdView);

		} else {
			holdView = (HoldView) convertView.getTag();
		}
		holdView.wallName.setText(list.get(position) +"安装应用赚钱");
		holdView.imageView.setImageResource(creatImg(list.get(position)));
		return convertView;
	}
	
	private int creatImg(String name){
		int i = R.drawable.u45;
		if("果盟".equals(name)){
			i = R.drawable.u45;
		}else if("多盟".equals(name)){
			i = R.drawable.u51;
		}else if("万普".equals(name)){
			i = R.drawable.u47;
		}else if("米迪".equals(name)){
			i = R.drawable.u49;
		}else if("有米".equals(name)){
			i = R.drawable.u48;
		}else if("安沃".equals(name)){
			i = R.drawable.u54;
		}
		return i;
	}

	static class HoldView {
		TextView wallName;
		ImageView imageView;
	}

}
