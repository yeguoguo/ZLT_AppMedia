package com.media.appmedia.activity;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.widget.RadioGroup;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.media.appmedia.R;
import com.media.appmedia.contarl.FragmentTabAdapter;
import com.media.appmedia.fragment.MessgMeFragment;
import com.media.appmedia.fragment.MessgSystemFragment;

/**
 * 
 * <pre>
 * 业务名:
 * 功能说明: 我的消息，简称 信封
 * 编写日期:	2014-11-7
 * 作者:	 Rose
 * 
 * 历史记录
 * 1、修改日期：
 *    修改人：
 *    修改内容：
 * </pre>
 */
public class EnvelopeActivity extends FragmentActivity {
	@ViewInject(R.id.radiogroup)
	private RadioGroup radioGroup;
	private ArrayList<Fragment> listFragments = new ArrayList<Fragment>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去掉标题栏
		setContentView(R.layout.envelope_layout);
		ViewUtils.inject(this);
		listFragments.add(new MessgSystemFragment());
		listFragments.add(new MessgMeFragment());
		new FragmentTabAdapter(getSupportFragmentManager(), listFragments,
				R.id.linearLayout_fragment_content, radioGroup);
	}

	@OnClick({ R.id.come_back})
	public void comeBack(View view) {
		EnvelopeActivity.this.finish();
	}

}
