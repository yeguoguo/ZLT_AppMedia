package com.media.appmedia.activity;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.media.appmedia.R;
import com.media.appmedia.network.ProtocolService;
import com.media.appmedia.util.MethodUtils;
import com.media.appmedia.util.PhoneInfoUtils;
import com.media.appmedia.util.ToastUtils;

/**
 * 
 * <pre>
 * 业务名:
 * 功能说明: 吃喝玩乐商城
 * 编写日期:	2014-10-22
 * 作者:	 Rose
 * 
 * 历史记录
 * 1、修改日期：
 *    修改人：
 *    修改内容：
 * </pre>
 */
public class ShopActivity extends Activity {
	@ViewInject(R.id.web_view_shoping)
	private WebView webView;
	private String url;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去掉标题栏
		setContentView(R.layout.shop_layout);
		ViewUtils.inject(this);
		
		getShoping();
	}
	/**
	 * 
	 * 方法说明：设置需要展示的WebView
	 *
	 * @param url
	 */
	private void setWebView(String url){
		WebSettings webSetting = webView.getSettings();
		webSetting.setJavaScriptEnabled(true);
		webView.requestFocusFromTouch();
		webView.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
		});
		webView.loadUrl(url);
	}

	@OnClick({ R.id.come_back})
	public void back(View v) {
		ShopActivity.this.finish();
	}
	
	/**
	 * 
	 * 方法说明：联网进行数据获取（获取webView的地址链接）
	 *
	 */
	private void getShoping(){
		ProtocolService.getShoping(PhoneInfoUtils.getIMEI(getApplicationContext()),
				new RequestCallBack<String>() {
					
					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						System.out.println("");
						try {
							JSONObject obj = new JSONObject(arg0.result);
							if(obj.getInt("message")==1){
								url = obj.getString("url");
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
						if( !MethodUtils.isEmpty(url)){
							setWebView(url);
						}
						
					}
					
					@Override
					public void onFailure(HttpException arg0, String arg1) {
						ToastUtils.show(getApplicationContext(),
								R.string.phone_verify_send_fail);
					}
				});
	}
	
}
