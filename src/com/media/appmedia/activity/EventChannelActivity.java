package com.media.appmedia.activity;


import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.media.appmedia.R;
import com.media.appmedia.network.ProtocolService;
import com.media.appmedia.util.PhoneInfoUtils;
import com.media.appmedia.util.ToastUtils;
/**
 * 
 * <pre>
 * 业务名:
 * 功能说明: 热门活动频道
 * 编写日期:	2014-10-22
 * 作者:	 Rose
 * 
 * 历史记录
 * 1、修改日期：
 *    修改人：
 *    修改内容：
 * </pre>
 */
public class EventChannelActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去掉标题栏
		setContentView(R.layout.eventchannel_layout);
		ViewUtils.inject(this);
		
		
	}
	
	@OnClick(R.id.sub)
	public void intentActivity(View v) {
		getIsSub();
	}
	@OnClick({ R.id.come_back})
	public void back(View v) {
		EventChannelActivity.this.finish();
	}
	
	
	
	private void getIsSub() {
		ProtocolService.getIsSub(
				PhoneInfoUtils.getIMEI(getApplicationContext()),
				new RequestCallBack<String>() {

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							JSONObject obj = new JSONObject(arg0.result);
							if (obj.getInt("message") == 1) {
								Intent intent = new Intent(EventChannelActivity.this, QuestionActivity.class);
								startActivity(intent);
							}else{
								ToastUtils.show(getApplicationContext(), "您已经参加过该活动！");
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
						System.out.println("");
					}

					@Override
					public void onFailure(HttpException arg0, String arg1) {

					}
				});
	}


	
}
