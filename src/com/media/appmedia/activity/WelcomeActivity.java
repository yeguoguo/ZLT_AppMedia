package com.media.appmedia.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.Window;
import cn.jpush.android.api.JPushInterface;

import com.alibaba.fastjson.JSONObject;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.media.appmedia.R;
import com.media.appmedia.base.BaseApplication;
import com.media.appmedia.entity.IsLoginBean;
import com.media.appmedia.network.ProtocolService;
import com.media.appmedia.util.MethodUtils;
import com.media.appmedia.util.NetUtil;
import com.media.appmedia.util.PhoneInfoUtils;
import com.media.appmedia.util.ToastUtils;

/**
 * 
 * <pre>
 * 业务名:
 * 功能说明: 欢迎界面
 * 编写日期:	2014-12-13
 * 作者:	 Rose
 * 
 * 历史记录
 * 1、修改日期：
 *    修改人：
 *    修改内容：
 * </pre>
 */
public class WelcomeActivity extends Activity {
	private String phone;
	private String mobId;

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == 1) {
				Intent intent = new Intent();
//				 取出存储本地的信息(存储了手机号码)
				phone = MethodUtils.getUserInfo(getApplicationContext());
				mobId = PhoneInfoUtils.getIMEI(getApplicationContext());
				if (phone != null && !"".equals(phone) && !"".equals(mobId)) {
					BaseApplication.getApplication().setUserPhone(phone);
					BaseApplication.getApplication().setMobId(mobId);
					// 启动主界面
					intent.setClass(WelcomeActivity.this, MainActivity.class);
					startActivity(intent);
					WelcomeActivity.this.finish();
				} else {
					if(NetUtil.isNetConn(getApplicationContext())){
						isLogn();
					}else{
						ToastUtils.show(getApplicationContext(), "网络无连接");
					}
				}
				
			}
		};
	};


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcome_layout);
		// 判断网络状态
		getHttpStadu();

	}

	AlertDialog alert;

	private void getHttpStadu() {

		alert = new AlertDialog.Builder(WelcomeActivity.this)
				.setTitle("提示")
				.setMessage("没有有效的网络连接，是否进行设置？")
				.setNegativeButton("设置网络",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								Intent intent = new Intent(
										Settings.ACTION_DATA_ROAMING_SETTINGS);
								startActivity(intent);
								WelcomeActivity.this.finish();
							}
						})
				.setPositiveButton("取消", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						WelcomeActivity.this.finish();
					}
				}).create();

		if (!isNetwork(WelcomeActivity.this)) {
			alert.show();
		} else {
			new Thread() {
				@Override
				public void run() {
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println();
					Message msg = new Message();
					msg.what = 1;
					handler.sendMessage(msg);
				}

			}.start();

		}

	}

	public boolean isNetwork(final Context context) {

		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = connectivityManager.getActiveNetworkInfo();
		if (info == null) {
			return false;
		}
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		JPushInterface.onResume(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		JPushInterface.onPause(getApplicationContext());
		
	}
	
	
	private void isLogn(){
		ProtocolService.isLogin(PhoneInfoUtils.getIMEI(getApplicationContext()), 
				new RequestCallBack<String>() {
					
					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						IsLoginBean bean = JSONObject.parseObject(arg0.result, IsLoginBean.class);
						if(!MethodUtils.isEmpty(bean.getTelephone())){
							Intent intent = new Intent(WelcomeActivity.this,
									MainActivity.class);
							startActivity(intent);
							MethodUtils.setUserInfo(
									getApplicationContext(), bean.getTelephone());
							BaseApplication.getApplication().setUserPhone(
									bean.getTelephone());
							WelcomeActivity.this.finish();
						}else{
							Intent intent = new Intent(WelcomeActivity.this,
									RegisterActivity.class);
							startActivity(intent);
							WelcomeActivity.this.finish();
							
						}
					}
					@Override
					public void onFailure(HttpException arg0, String arg1) {
						ToastUtils.show(getApplicationContext(), "系统错误");
					}
				});
	}
}
