package com.media.appmedia.activity;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.media.appmedia.R;
import com.media.appmedia.base.BaseActivity;
import com.media.appmedia.network.ProtocolService;
import com.media.appmedia.util.MethodUtils;
import com.media.appmedia.util.PhoneInfoUtils;
import com.media.appmedia.util.ToastUtils;

public class QuestionActivity extends BaseActivity implements
		OnCheckedChangeListener,
		android.widget.CompoundButton.OnCheckedChangeListener{
	private String age;
	private String xinzi;
	private String aihao;
	private String sex;
	private String culture;
	private String trade;
	private String sjlxing;
	private String sjbzhi;
	private String opinion;
	@ViewInject(R.id.group_age)
	private RadioGroup radio_age;
	@ViewInject(R.id.group_sex)
	private RadioGroup radio_sex;
	@ViewInject(R.id.group_culture)
	private RadioGroup radio_culture;
	@ViewInject(R.id.group_xinzi)
	private RadioGroup radio_xinzi;
	@ViewInject(R.id.group_trade)
	private RadioGroup radio_trade;
	@ViewInject(R.id.edittext_opinion)
	private EditText editText;
	@ViewInject(R.id.check_aihao_01)
	private CheckBox checkBox_aihao_01;
	@ViewInject(R.id.check_aihao_02)
	private CheckBox checkBox_aihao_02;
	@ViewInject(R.id.check_aihao_03)
	private CheckBox checkBox_aihao_03;
	@ViewInject(R.id.check_aihao_04)
	private CheckBox checkBox_aihao_04;
	@ViewInject(R.id.check_sjlxing_01)
	private CheckBox checkBox_sjlx_01;
	@ViewInject(R.id.check_sjlxing_02)
	private CheckBox checkBox_sjlx_02;
	@ViewInject(R.id.check_sjlxing_03)
	private CheckBox checkBox_sjlx_03;
	@ViewInject(R.id.check_sjlxing_04)
	private CheckBox checkBox_sjlx_04;
	@ViewInject(R.id.check_sjlxing_05)
	private CheckBox checkBox_sjlx_05;
	@ViewInject(R.id.check_sjlxing_06)
	private CheckBox checkBox_sjlx_06;
	@ViewInject(R.id.check_sjlxing_07)
	private CheckBox checkBox_sjlx_07;
	@ViewInject(R.id.check_sjlxing_08)
	private CheckBox checkBox_sjlx_08;
	@ViewInject(R.id.check_sjlxing_09)
	private CheckBox checkBox_sjlx_09;
	@ViewInject(R.id.check_sjbzhi_01)
	private CheckBox checkBox_sjbz_01;
	@ViewInject(R.id.check_sjbzhi_02)
	private CheckBox checkBox_sjbz_02;
	@ViewInject(R.id.check_sjbzhi_03)
	private CheckBox checkBox_sjbz_03;
	@ViewInject(R.id.check_sjbzhi_04)
	private CheckBox checkBox_sjbz_04;
	@ViewInject(R.id.check_sjbzhi_05)
	private CheckBox checkBox_sjbz_05;
	@ViewInject(R.id.check_sjbzhi_06)
	private CheckBox checkBox_sjbz_06;
	@ViewInject(R.id.check_sjbzhi_07)
	private CheckBox checkBox_sjbz_07;
	@ViewInject(R.id.check_sjbzhi_08)
	private CheckBox checkBox_sjbz_08;
	@ViewInject(R.id.check_sjbzhi_09)
	private CheckBox checkBox_sjbz_09;
	
	private ArrayList<String> listLike = new ArrayList<String>();
	private ArrayList<String> listSjlxing = new ArrayList<String>();
	private ArrayList<String> listSjbzhi = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_questionnaire);
		ViewUtils.inject(this);
		setListener();
	}

	private void setListener() {
		//RadioGroup的监听设置
		radio_age.setOnCheckedChangeListener(this);
		radio_culture.setOnCheckedChangeListener(this);
		radio_sex.setOnCheckedChangeListener(this);
		radio_trade.setOnCheckedChangeListener(this);
		radio_xinzi.setOnCheckedChangeListener(this);
		//EditText的监听设置
		editText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				opinion = editText.getText().toString().trim();
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				
			}
		});
		//CheckBox的监听设置
		checkBox_aihao_01.setOnCheckedChangeListener(this);
		checkBox_aihao_02.setOnCheckedChangeListener(this);
		checkBox_aihao_03.setOnCheckedChangeListener(this);
		checkBox_aihao_04.setOnCheckedChangeListener(this);
		checkBox_sjlx_01.setOnCheckedChangeListener(this);
		checkBox_sjlx_02.setOnCheckedChangeListener(this);
		checkBox_sjlx_03.setOnCheckedChangeListener(this);
		checkBox_sjlx_04.setOnCheckedChangeListener(this);
		checkBox_sjlx_05.setOnCheckedChangeListener(this);
		checkBox_sjlx_06.setOnCheckedChangeListener(this);
		checkBox_sjlx_07.setOnCheckedChangeListener(this);
		checkBox_sjlx_08.setOnCheckedChangeListener(this);
		checkBox_sjlx_09.setOnCheckedChangeListener(this);
		checkBox_sjbz_01.setOnCheckedChangeListener(this);
		checkBox_sjbz_02.setOnCheckedChangeListener(this);
		checkBox_sjbz_03.setOnCheckedChangeListener(this);
		checkBox_sjbz_04.setOnCheckedChangeListener(this);
		checkBox_sjbz_05.setOnCheckedChangeListener(this);
		checkBox_sjbz_06.setOnCheckedChangeListener(this);
		checkBox_sjbz_07.setOnCheckedChangeListener(this);
		checkBox_sjbz_08.setOnCheckedChangeListener(this);
		checkBox_sjbz_09.setOnCheckedChangeListener(this);
		

	}
	/**
	 * RadioGroup的单选按钮结果监听
	 */
	@Override
	public void onCheckedChanged(RadioGroup radioGroup, int checkId) {
		RadioButton radioButton = (RadioButton) findViewById(checkId);
		String str = radioButton.getText().toString().trim();
		switch (radioGroup.getId()) {
		case R.id.group_age:
			if("18岁以下".equals(str)){
				str = "18周岁以下";
			}else if("18—25岁".equals(str)){
				str = "19 - 25周岁";
			}else if("26—50岁".equals(str)){
				str = "26 - 50周岁";
			}else if("51岁以上".equals(str)){
				str = "51周岁之上";
			}
			age = str;
			break;
		case R.id.group_culture:
			if("硕士以上".equals(str)){
				culture = "硕士之上";
			}else{
				culture = str;
			}
			break;
		case R.id.group_sex:
			sex = str;
			break;
		case R.id.group_trade:
			trade = str;
			break;
		case R.id.group_xinzi:
			xinzi = str;
			break;
		}
		System.out.println("");
	}
	
	

	@OnClick({ R.id.come_back})
	public void back(View v) {
		QuestionActivity.this.finish();
	}
	/**
	 * 
	 * 方法说明：发送点击事件进行信息上传服务器
	 *
	 * @param view
	 */
	@OnClick(R.id.btton_submit)
	public void sendMessg(View view) {
		packList();
		if (MethodUtils.isEmpty(trade) || MethodUtils.isEmpty(age)
				|| MethodUtils.isEmpty(xinzi) || MethodUtils.isEmpty(aihao)
				|| MethodUtils.isEmpty(sex) || MethodUtils.isEmpty(culture)
				|| MethodUtils.isEmpty(sjlxing) || MethodUtils.isEmpty(opinion)
				|| MethodUtils.isEmpty(sjbzhi) ) {

			ToastUtils.show(getApplicationContext(), "请填写完整信息，谢谢配合！");
			return;
		}else if(opinion.length()<10){
			ToastUtils.show(getApplicationContext(), "用户建议字数要求大于10个字符！");
			return ;
		}
		//信息填写完整后进行上传服务器
		putSub(age, xinzi, aihao, sex, culture, trade, sjlxing, sjbzhi, opinion);

	}

	/**
	 * 
	 * 方法说明：用于发送用户填写的调查信息
	 * 
	 * @param question_search_age
	 * @param question_search_xinzi
	 * @param question_search_aihao
	 * @param question_search_sex
	 * @param question_search_culture
	 * @param question_search_trade
	 * @param question_search_sjlxing
	 * @param question_search_sjbzhi
	 * @param question_search_opinion
	 */
	private void putSub(String question_search_age,
			String question_search_xinzi, String question_search_aihao,
			String question_search_sex, String question_search_culture,
			String question_search_trade, String question_search_sjlxing,
			String question_search_sjbzhi, String question_search_opinion) {
		ProtocolService.submitQestion(
				PhoneInfoUtils.getIMEI(getApplicationContext()),
				question_search_age, question_search_xinzi,
				question_search_aihao, question_search_sex,
				question_search_culture, question_search_trade,
				question_search_sjlxing, question_search_sjbzhi,
				question_search_opinion, new RequestCallBack<String>() {

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							JSONObject obj = new JSONObject(arg0.result);
							if(obj.getInt("message") == 1){
								ToastUtils.show(getApplicationContext(), "提交成功，系统将为您账户增加金币！");
								QuestionActivity.this.finish();
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println();
					}

					@Override
					public void onFailure(HttpException arg0, String arg1) {

					}
				});
	}
	/**
	 * CheckBox的回调监听
	 */
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		String str = buttonView.getText().toString().trim();
		switch (buttonView.getId()) {
		case R.id.check_aihao_01:
		case R.id.check_aihao_02:
		case R.id.check_aihao_03:
		case R.id.check_aihao_04:
			isOperate(listLike, changString(str), isChecked);
			break;
		case R.id.check_sjlxing_01:
		case R.id.check_sjlxing_02:
		case R.id.check_sjlxing_03:
		case R.id.check_sjlxing_04:
		case R.id.check_sjlxing_05:
		case R.id.check_sjlxing_06:
		case R.id.check_sjlxing_07:
		case R.id.check_sjlxing_08:
		case R.id.check_sjlxing_09:
			isOperate(listSjlxing, str, isChecked);
			break;
		case R.id.check_sjbzhi_01:
		case R.id.check_sjbzhi_02:
		case R.id.check_sjbzhi_03:
		case R.id.check_sjbzhi_04:
		case R.id.check_sjbzhi_05:
		case R.id.check_sjbzhi_06:
		case R.id.check_sjbzhi_07:
		case R.id.check_sjbzhi_08:
		case R.id.check_sjbzhi_09:
			isOperate(listSjbzhi, str, isChecked);
			break;

		}
	}
	
	private String changString(String str){
		String strs = null;
		if("爱吃".equals(str)){
			strs = "吃";
		}else if("爱玩".equals(str)){
			strs = "玩";
			
		}else if("爱穿".equals(str)){
			strs = "穿";
			
		}else if("爱购".equals(str)){
			strs = "购";
			
		}
		return strs;
		
	}
	/**
	 * 
	 * 方法说明：暂时存储CheckBox的内容
	 *
	 * @param list	当前操作按钮的种类
	 * @param str	当前操作的按钮的内容
	 * @param flage	是选中还是取消
	 */
	private void isOperate(ArrayList<String> list,String str,boolean flage){
		if(flage){
			addList(list, str);
		}else{
			removesList(list, str);
		}
	}
	private void addList(ArrayList<String> list,String str){
		list.add(str);
	}
	private void removesList(ArrayList<String> list,String str){
		ArrayList<String> removeList = new ArrayList<String>();
		removeList.add(str);
		list.removeAll(removeList);
	}
	
	private void packList(){
		aihao = "";
		for(int i =0;i<listLike.size();i++){
			aihao += listLike.get(i)+",";
		}
		aihao = aihao.substring(0, aihao.length()!=0?aihao.length()-1:0);
		
		sjlxing = "";
		for(int i = 0;i<listSjlxing.size();i++){
			sjlxing += listSjlxing.get(i) +",";
		}
		sjlxing = sjlxing.substring(0, sjlxing.length()!=0?sjlxing.length()-1:0);
		
		sjbzhi = "";
		for(int i=0;i<listSjbzhi.size();i++){
			sjbzhi += listSjbzhi.get(i) + ",";
		}
		sjbzhi = sjbzhi.substring(0, sjbzhi.length() !=0?sjbzhi.length()-1:0);
	}
}
