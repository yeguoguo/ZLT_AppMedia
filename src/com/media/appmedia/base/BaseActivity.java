package com.media.appmedia.base;

import org.json.JSONException;

import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.media.appmedia.R;
import com.media.appmedia.activity.ShareActivity2;
import com.media.appmedia.network.ProtocolConst;
import com.media.appmedia.network.ProtocolService;
import com.media.appmedia.util.PhoneInfoUtils;
import com.media.appmedia.util.ToastUtils;
import com.umeng.socialize.bean.RequestType;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.bean.SocializeEntity;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.controller.listener.SocializeListeners.SnsPostListener;
import com.umeng.socialize.media.QQShareContent;
import com.umeng.socialize.media.QZoneShareContent;
import com.umeng.socialize.media.RenrenShareContent;
import com.umeng.socialize.media.SinaShareContent;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.sso.EmailHandler;
import com.umeng.socialize.sso.QZoneSsoHandler;
import com.umeng.socialize.sso.RenrenSsoHandler;
import com.umeng.socialize.sso.SinaSsoHandler;
import com.umeng.socialize.sso.UMQQSsoHandler;
import com.umeng.socialize.sso.UMSsoHandler;
import com.umeng.socialize.weixin.controller.UMWXHandler;
import com.umeng.socialize.weixin.media.CircleShareContent;
import com.umeng.socialize.weixin.media.WeiXinShareContent;

import cn.jpush.android.api.JPushInterface;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Window;
import android.widget.Toast;

/**
 * 
 * 作者: Rose 业务名: 功能说明: 用于基本activity集成，实现一些集成功能 编写日期: 2014-12-5
 * 
 */
public class BaseActivity extends Activity {
	private final String SHARE_TITLE = "赚乐淘";
	final UMSocialService mController = UMServiceFactory.getUMSocialService(
			"com.umeng.share", RequestType.SOCIAL);
	private String myUrl;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去掉标题栏
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onResume() {
		super.onResume();
		JPushInterface.onResume(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		JPushInterface.onPause(getApplicationContext());
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		/** 使用SSO授权必须添加如下代码 */
		UMSsoHandler ssoHandler = mController.getConfig().getSsoHandler(
				requestCode);
		if (ssoHandler != null) {
			ssoHandler.authorizeCallBack(requestCode, resultCode, data);
		}

	}

	/**
	 * 初始化SDK，添加一些平台
	 */
	public void initSocialSDK(String shareContent,final String type) {
		myUrl = getMyShare(PhoneInfoUtils.getIMEI(getApplicationContext()));
		Bitmap bitmaps = BitmapFactory.decodeResource(getResources(),
				R.drawable.ic_launcher);
		mController.setShareContent(shareContent);
		// 图片分享内容
		mController.setShareMedia(new UMImage(BaseActivity.this, bitmaps));
		// 添加微信朋友圈
		String appID = "wx05f3d08ee1c4a3a0";
		String appSecret = "09470be5f3cbfc4113320a6360a85807";
		UMWXHandler wxCircleHandler = new UMWXHandler(getApplicationContext(),
				appID, appSecret);
		wxCircleHandler.setToCircle(true);
		wxCircleHandler.addToSocialSDK();

		// 设置微信朋友圈分享内容
		CircleShareContent circleMedia = new CircleShareContent();
		
		circleMedia.setShareContent(shareContent + myUrl);
		// 设置朋友圈title
		circleMedia.setTargetUrl(myUrl);
//		circleMedia.setTitle("升官发财\n" + shareContent );
//		circleMedia
//				.setShareImage(new UMImage(getApplicationContext(), bitmaps));
		mController.setShareMedia(circleMedia);

		mController.directShare(getApplicationContext(),
				SHARE_MEDIA.WEIXIN_CIRCLE, new SnsPostListener() {

					@Override
					public void onStart() {
						// TODO Auto-generated method stub
					}

					@Override
					public void onComplete(SHARE_MEDIA platform, int stCode,
							SocializeEntity entity) {
						// TODO Auto-generated method stub
						// 分享完回调...成功或失败！
						if (stCode == 200) {
							putShareGolds(PhoneInfoUtils
									.getIMEI(getApplicationContext()), type,
									"23");
							Toast.makeText(BaseActivity.this, "分享成功！！！",
									Toast.LENGTH_SHORT).show();
							System.out.println("分享成功！！！");
						} else {
							Toast.makeText(BaseActivity.this,
									"分享失败 : error code : " + stCode,
									Toast.LENGTH_SHORT).show();
						}

					}
				});
	}

	public void openShare() {
		// mController.getConfig().setPlatforms(
		// SHARE_MEDIA.WEIXIN_CIRCLE);
		// mController.openShare(BaseActivity.this, false);
	}

	/**
	 * 
	 * 方法说明：得到个人推广链接
	 * 
	 * @param biaoshifu
	 * @return
	 */
	public String getMyShare(String biaoshifu) {
		return ProtocolConst.MAIN + "index.php?biaoshifu=" + biaoshifu;
	}

	private void putShareGolds(String biaoshifu, String share_record_content,
			String share_record_type) {
		ProtocolService.putShareGold(biaoshifu, share_record_content,
				share_record_type, new RequestCallBack<String>() {
					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
					}

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						ToastUtils.show(getApplicationContext(),
								R.string.phone_verify_send_fail);
					}
				});
	}

}
