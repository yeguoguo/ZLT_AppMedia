package com.media.appmedia.base;

/**
 * 
 * 作者: Rose 业务名:用于存储用户信息（单例） 功能说明: 用于存储用户信息，方便展示 编写日期: 2014-11-28
 * 
 */
public class UserInfo {
	private String userName;
	// 当前积分数
	private String integ;
	// 历史消费积分
	private String history_grande;
	// 当前剩余积分
	private String integral_grande;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getInteg() {
		return integ;
	}

	public void setInteg(String integ) {
		this.integ = integ;
	}

	public String getHistory_grande() {
		return history_grande;
	}

	public void setHistory_grande(String history_grande) {
		this.history_grande = history_grande;
	}

	public String getIntegral_grande() {
		return integral_grande;
	}

	public void setIntegral_grande(String integral_grande) {
		this.integral_grande = integral_grande;
	}

	public static void setUserInfo(UserInfo userInfo) {
		UserInfo.userInfo = userInfo;
	}

	private static UserInfo userInfo;

	public static UserInfo getUserInfo() {
		if (userInfo == null) {
			userInfo = new UserInfo();
		}
		return userInfo;
	}
}
