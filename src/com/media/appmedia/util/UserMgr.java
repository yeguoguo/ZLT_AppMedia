package com.media.appmedia.util;

/**
 * 用户信息管理类
 */
public class UserMgr {

	/** 用户是否登录 */
	private boolean isLogin;
	/** 登录账号 */
	private String userID;
	/** 登录密码 */
	// private String password;
	/** 用户详细信息实体类 */
	// private UserInfo userInfo;
	/** 登录记录存储类 */
	private static UserMgr instance = null;

	private UserMgr() {
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	/*
	 * public String getPassword() { return password; }
	 * 
	 * public void setPassword(String pwd) { this.password = pwd; }
	 */
	public boolean isLogin() {
		return isLogin;
	}

	public void setLogin(boolean flag) {
		isLogin = flag;
	}

	public void cancel() {
		setUserID(null);
		// setPassword(null);
		// setUserInfo(null);
		setLogin(false);
	}

	/*
	 * public void setUserInfo(UserInfo userInfo) { this.userInfo = userInfo; if
	 * (userInfo != null) { setUserID(userInfo.getUserCode()); setLogin(true); }
	 * }
	 * 
	 * public UserInfo getUserInfo() { return userInfo; }
	 */
	public static UserMgr getInstance() {
		if (instance == null)
			instance = new UserMgr();
		return instance;
	}

	public void gc() {
		cancel();
		instance = null;
	}
}
