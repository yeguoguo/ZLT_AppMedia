package com.media.appmedia.util;

public class Const {

	/** 应用包名 */
	public static final String PKG_NAME = "pkg_name";
	/** 文件ID */
	public static final String FILE_ID = "file_id";
	/** 图片位置索引 */
	public static final String PIC_INDEX = "picIndex";
	/** 图片编号数组 */
	public static final String PIC_IDS = "picIds";
	/** 上传下载文件实体名 */
	public static final String FILE_BEAN = "file_bean";
	/** 外部存储设备(SD卡)标识 */
	public static final int EXTERNAL_DEVICE_FLAG = 1;
	/** 内存存储设备标识 */
	public static final int INTERNAL_DEVICE_FLAG = 2;
	/** 文件下载进度(百分比) */
	public static final String FILE_DL_PCT = "file_download_percent";
	/** 文件已下载大小 */
	public static final String FILE_DL_SIZE = "file_download_size";

	/** 绑定手机验证码最长等待时间 */
	public static final int MAX_GET_PHONE_NUMBER_TIMER = 60;
	/** 密码最低长度限制 */
	public static final int MIN_PWD_LENGTH = 6;

	/** 注册成功标识 */
	public static final int REGISTER_SUCCESS = 100;
	/** 登录成功标识 */
	public static final int LOGIN_SUCCESS = 101;
	/** 完善个人资料成功标识 */
	public static final int CPT_PERSON_INFO_SUCCESS = 102;

	/** 头像大小 */
	public static final int HEAD_SIZE = 120;

	/** 上传身份证图片的大小 */
	public static final int INDENTIFY = 1280;

	/** 禁用用户操作代号 */
	public static final int OFF_USER_ERROR_NUM = 9;

	/** 普通用户发记录的最大图片数 **/
	public static final int MAX_RECORD_SIZE = 15;

	// ====================用户信息审核=====================
	// 0:未填写 1: 待审核 2:审核通过 3：审核不通过
	/** 未完善个人资料 */
	public static final int NONE_COMPLETE_USER_INFO = 0;
	/** 用户信息待审核 */
	public static final int VERIFY_WAIT_USER_INFO = 1;
	/** 用户信息审核通过 */
	public static final int VERIFY_PASS_USER_INFO = 2;
	/** 用户信息审核不通过 */
	public static final int VERIFY_NO_PASS_USER_INFO = 3;
	// ====================================================

	// =======================发送验证码类型=================
	/** 注册获取手机验证码 */
	public static final int REGISTER_VERIFY_CODE_TYPE = 1;
	/** 找回密码获取手机验证码 */
	public static final int FIND_PWD_VERIFY_CODE_TYPE = 2;
	// =====================================================

	// =======================用户角色======================
	/** 专业摄影师 */
	public static final int ROLE_PROFESSION_CAMERAMAN = 1;
	/** 摄影达人 */
	public static final int ROLE_CAMERAMAN = 2;
	/** 特约通讯员 */
	public static final int ROLE_CORRESPONDENT = 3;
	/** 其他（组织、机构） */
	public static final int ROLE_OTHER = 4;
	/** 普通用户 */
	public static final int ROLE_COMMON = 5;
	// =====================================================
	/** 消息通知 */
	public static final int NOTICE = 1;
	/** 消息提醒 */
	public static final int REMIND = 2;
	/** 赞 */
	public static final int PRISE = 3;

	// ======================================================

	/**
	 * 配置文件名称
	 */
	public static final String SettingInfoDown3 = "setting_info3";
	public static final String SettingInfoDown2 = "setting_info2";
	public static final String SettingInfoDown1 = "setting_info1";
	public static final String UserInfoDown = "user_info";
	public static final String SHAREPREFRENCE = "zy_config";
	public static final String IMG_DOWNLOAD_PATH = "img_download_path";
	public static final String NOTICE_UPDATE = "notice_update";
	public static final String AUTO_LOGIN = "auto_login";
	public static final String LOCK_INFO = "lock_info";
	public static final String MESSGE = "messge";
	public static final String SHARE = "share_info";

	/**
	 * 分类列表数据
	 */
	// public static ArrayList<CategoryDatas> mCategoryList;

	public static final int NOTHING = 0;
	public static final int UPLOADING = 1;
	public static final int PAUSE = 2;

	public static final int UPDATE = 100;
	public static final int DELETE = 101;

	public static final int PUBLISHTASK = 203;// 发布的任务
	public static final int PUBLISHDRAFT = 204;// 发布的任务

	/**
	 * 消息
	 */
	public static final int AUITBY = 100;// 审核通过提醒

	public static final int AUITNOT = 101;// 审核不通过提醒

	public static final int TRADINGCLOSE = 102;// 是直播、报道关闭

	public static final int TRADINGREMIND = 103;// 交易提醒

	public static final int DELETEPIC = 104;// 删除图片提醒

	public static final int LIVINGREMIND = 105;// 直播提醒

	public static final int RECOMMENDREMIND = 106;// 推荐提醒

	public static final int TOPICREMIND = 107;// 专题推荐提醒

	/** 标识Aviary美化后进入界面 **/
	public static final int AVIARY_LIVE = 0x101; // 默认值
	public static final int AVIARY_RECORD = 0x102;

}