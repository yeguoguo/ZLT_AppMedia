package com.media.appmedia.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.media.appmedia.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

/**
 * 字符串工具类
 * 
 * @author welen
 * 
 */
public class StringUtils {

	/**
	 * 判断字符串是否为空
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		if (str == null || str.equalsIgnoreCase("null")
				|| str.trim().length() == 0)
			return true;
		else
			return false;
	}

	/**
	 * 获得自定义字符串
	 * 
	 * @param str
	 * @return
	 */
	public static SpannableString getDiffStr(String str, int color, int start,
			int end) {
		SpannableString ss = new SpannableString(str);
		ss.setSpan(new ForegroundColorSpan(color), start, end,
				Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		return ss;
	}

	/**
	 * 获取数字的位数
	 * 
	 * @param num
	 * @return
	 */
	public static int getLen(int num) {
		String target = String.valueOf(num);
		return target.length();
	}

	@SuppressLint("SimpleDateFormat")
	public static long getTime(String time) {
		if (time == null)
			return new Date().getTime();
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date d = date.parse(time);
			return d.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static long getTime(String time, String patten) {

		SimpleDateFormat date = new SimpleDateFormat(patten);
		try {
			Date d = date.parse(time);
			return d.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static String getDate(long time) {
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
		Date d = new Date(time);
		return date.format(d);
	}

	public static String getPreDate(Context ctx, long time) {

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) - 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
		long second = time / 1000;
		if (second < 60) {
			return second + ctx.getResources().getString(R.string.second);
		} else if (second >= 60 && second < 3600) {
			return second / 60 + ctx.getResources().getString(R.string.minute);
		} else if (second >= 3600 && second < 3600 * 24) {
			return second / 3600 + ctx.getResources().getString(R.string.hour);
		} else {
			cal.setTimeInMillis(time);
			return cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1)
					+ "-" + cal.get(Calendar.DAY_OF_MONTH);
		}

		/*
		 * SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd"); Date d =
		 * new Date(time); return date.format(d);
		 */
	}

	public static String getDate(long time, String pattren) {
		SimpleDateFormat date = new SimpleDateFormat(pattren);
		Date d = null;
		if (time <= 0) {
			d = new Date();
		} else {
			d = new Date(time);
		}
		return date.format(d);
	}

	/**
	 * 毫秒转日期字符串
	 * 
	 * @param str
	 *            毫秒数字符串
	 * @return
	 */
	public static String getDateTimeByMillisecond(String str) {
		Date date = new Date(Long.valueOf(str));
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String time = format.format(date);
		return time;
	}

	/**
	 * 毫秒转年龄字符串
	 * 
	 * @param str
	 *            毫秒数字符串
	 * @return
	 */
	public static String getAgeByMillisecond(String birthdayTime,
			String serverTime) {
		Date date = new Date(Long.valueOf(birthdayTime));
		if (serverTime == null || serverTime.equals("")) {
			return "0";
		}
		long i = (Long.valueOf(serverTime) - date.getTime())
				/ (1000 * 60 * 60 * 24);
		if (i <= 0) {
			return "0";
		}
		return Long.toString(i / 365);
	}

	/**
	 * 年龄转毫秒字符串
	 * 
	 * @param str
	 *            毫秒数字符串
	 * @return
	 */
	public static long getMillisecondByAge(String age, String serverTime) {
		Date date = null;
		if (serverTime == null || serverTime.equals("")) {
			date = new Date(Long.valueOf(System.currentTimeMillis()));
		} else {
			date = new Date(Long.valueOf(serverTime));
		}
		date.setYear(date.getYear() - (Integer.valueOf(age)));
		MLog.d("year", date.getYear() + "");
		return date.getTime();
	}

	/**
	 * 毫秒转日期字符串
	 * 
	 * @param mill
	 *            毫秒数
	 * @return
	 */
	public static String getDateTimeByMillisecond(long mill) {
		Date date = new Date(Long.valueOf(mill));
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String time = format.format(date);
		return time;
	}

	/**
	 * 毫秒转日期字符串
	 * 
	 * @param mill
	 *            毫秒数
	 * @return
	 */
	public static String getDateByMillisecond(long mill) {
		Date date = new Date(Long.valueOf(mill));
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String time = format.format(date);
		return time;
	}

	/** 获取当前日期 */
	public static String getCurDate() {
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date d = new Date();
		return date.format(d);
	}

	/**
	 * 功能：判断字符串是否为日期格式
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isDate(String strDate) {
		Pattern pattern = Pattern
				.compile("^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\\s(((0?[0-9])|([1-2][0-3]))\\:([0-5]?[0-9])((\\s)|(\\:([0-5]?[0-9])))))?$");
		Matcher m = pattern.matcher(strDate);
		if (m.matches()) {
			return true;
		} else {
			return false;
		}
	}

	/** 检查字符串长度 */
	public static boolean checkStrLen(String str, int low, int hight) {
		if (str == null || hight < 0 || low < 0) {
			return false;
		}
		int len = str.length();
		if (len > hight || len < low) {
			return false;
		}
		return true;
	}

}
