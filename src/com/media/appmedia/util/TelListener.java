package com.media.appmedia.util;
import com.media.appmedia.base.BaseApplication;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

public class TelListener extends PhoneStateListener {  
    private Context context;  
    public TelListener(Context context){  
        this.context = context;  
    }  
  
    @Override  
    public void onCallStateChanged(int state, String incomingNumber) {  
        super.onCallStateChanged(state, incomingNumber);  
        BaseApplication.getApplication().setSetLock(false);
        if(state == TelephonyManager.CALL_STATE_RINGING){  
        	BaseApplication.getApplication().stopLockService();
            BaseApplication.getApplication().finishLock();
        }else if(state==TelephonyManager.CALL_STATE_OFFHOOK){
        	BaseApplication.getApplication().finishLock();
        	BaseApplication.getApplication().stopLockService();
        }else if(state == TelephonyManager.CALL_STATE_IDLE){  
        	BaseApplication.getApplication().setSetLock(true);
        	BaseApplication.getApplication().startLockService();
        }
    }

}  