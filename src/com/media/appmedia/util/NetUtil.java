package com.media.appmedia.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

/**
 * 工具类
 * 
 * @author lizhenwen
 * @date 2014-3-26
 */
public class NetUtil {

	private Context mContext;

	public NetUtil(Context context) {
		mContext = context;
	}

	/**
	 * 判断网络状态
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isNetConn(Context context) {
		ConnectivityManager connMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = connMgr.getActiveNetworkInfo();
		if (netInfo == null || !netInfo.isAvailable()) {
			return false;
		}
		return true;
	}

	/**
	 * 通过url利用HttpUrlConntion来获取输入流
	 * 
	 * @param urlStr
	 * @return
	 */
	public static InputStream getInputStreamFromUrlByHttpUrlConntion(
			String urlStr) {
		InputStream is = null;

		try {
			URL url = new URL(urlStr);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(5 * 1000);
			conn.setConnectTimeout(5 * 1000);
			if (conn.getResponseCode() == 200) {
				is = conn.getInputStream();
			} else {
				Log.e("TAG", "服务器异常！");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return is;
	}

	/**
	 * 通过url利用HttpUrlConnection来获取网络上的图片
	 * 
	 * @param urlStr
	 * @return
	 */
	public static Bitmap getBitmapFromUrlByHttpUrlConnection(String urlStr) {
		Bitmap resultBit = null;
		HttpURLConnection conn = null;
		try {
			URL url = new URL(urlStr);
			conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(5 * 1000);
			conn.setConnectTimeout(5 * 1000);
			if (conn.getResponseCode() == 200) {
				resultBit = BitmapFactory.decodeStream(conn.getInputStream());
			} else {
				Log.i("TAG", "加载失败");
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.disconnect();
				conn = null;
			}
		}
		return resultBit;
	}

	public static String getStringFromUrlByHttpClient(String urlStr) {
		String resultStr = null;
		HttpGet request = new HttpGet(urlStr);
		HttpParams connParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(connParams, 5 * 1000);
		HttpConnectionParams.setSoTimeout(connParams, 5 * 1000);
		HttpClient client = new DefaultHttpClient(connParams);
		try {
			HttpResponse response = client.execute(request);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				HttpEntity entity = response.getEntity();
				resultStr = EntityUtils.toString(entity, "utf-8");
			} else {
				Log.e("TAG", "服务器响应失败！");
			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultStr;
	}

	/**
	 * 通过url用HttpClient来获取输入流
	 * 
	 * @param urlStr
	 * @return
	 */
	public static InputStream getInutStreamFromUrlByHttpClient(String urlStr) {
		InputStream is = null;
		HttpGet request = new HttpGet(urlStr);
		HttpParams connParams = new BasicHttpParams();
		HttpConnectionParams.setSoTimeout(connParams, 5 * 1000);
		HttpConnectionParams.setConnectionTimeout(connParams, 5 * 1000);
		HttpClient client = new DefaultHttpClient(connParams);
		try {
			HttpResponse response = client.execute(request);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				HttpEntity entity = response.getEntity();
				is = entity.getContent();
			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return is;
	}
}
