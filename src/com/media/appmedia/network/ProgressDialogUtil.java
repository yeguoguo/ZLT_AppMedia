package com.media.appmedia.network;

import com.media.appmedia.R;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * 请求加载Dialog封装类
 */
public class ProgressDialogUtil {

	private static ProgressDialog pgsDialog = null;

	public static void showDialog(Context ctx) {
		pgsDialog = new ProgressDialog(ctx);
		pgsDialog.setCancelable(false);
		pgsDialog.setMessage(ctx.getResources().getString(R.string.wait));
		pgsDialog.setCanceledOnTouchOutside(true);
		pgsDialog.show();
	}

	public static void closeDialog() {
		if (pgsDialog != null) {
			pgsDialog.dismiss();
			pgsDialog = null;
		}
	}

}
