package com.media.appmedia.network;
/**
 * 
 * 作者:	Rose
 * 业务名:	请求地址类
 * 功能说明: 	提供所有联网请求地址
 * 编写日期:	2014-12-4
 *
 */
public class ProtocolConst {
	/** 版本更新apk下载地址 */
	public static final String VERSION_DOWLOAC = "http://www.sgfc88.com/Public/";
	/** 主页 */
	public static final String MAIN = "http://www.sgfc88.com/";
	/** V发送手机验证码接口 */
	public static final String USER_VERFIYCODE = MAIN+"android.php/Yscm/index";
	/** 提交验证码，注册接口 */
	public static final String REGIST_REGISTER = MAIN+"android.php/Yscm/register";
	/** 侧滑我的信息 */
	public static final String My_URL = MAIN+"/android.php/Yscm/message";
	/** 累计接口 */
	public static final String LEIJI_URL = MAIN+"android.php/Yscm/history";
	/** 即时等级积分获取接口 */
	public static final String MY_GRADE = MAIN+"android.php/Homepage/mess";
	/** 获取签到抽奖的状态 */
	public static final String IS_SIGN = MAIN+"android.php/Homepage/and_sign_record";
	/** 发送抽奖结果 */
	public static final String POST_RMB = MAIN+"android.php/Homepage/and_sign_record_add";
	/** 获得的积分详情 */
	public static final String GET_POINTS = MAIN+"android.php/Yscm/and_get_integral";
	/** 消费积分详情 */
	public static final String PUT_POINTS = MAIN+"android.php/Yscm/and_sonsume_integral";
	/** 积分(历史和当前的积分)数据 */
	public static final String JIFEN = MAIN+"android.php/Yscm/history";
	/** 获取好友推广信息数据 */
	public static final String FRIENDS = MAIN+"android.php/Yscm/friend_expand";
	/** 获取好友数量数据 */
	public static final String FRIENDS_COUNT = MAIN+"android.php/Yscm/friend_expand_order";
	/** 获取我的消息数据 */
	public static final String MY_MESSGES = MAIN+"android.php/Yscm/and_my_content";
	/** 获取系统消息数据 */
	public static final String SYSTEM_MESSGES = MAIN+"android.php/Yscm/and_system_content";
	/** QQ兑换服务 */
	public static final String QQ_PAY = MAIN+"android.php/Convent/and_qbi_record";
	/** 手机话费兑换服务 */
	public static final String PHONE_PAY = MAIN+"android.php/Convent/and_bill_record";
	/** 支付寶兑换服务 */
	public static final String ALI_PAY = MAIN+"android.php/Convent/and_alipay_record";
	/** 财付通兑换服务 */
	public static final String TEN_PAY = MAIN+"android.php/Convent/and_tenpay_record";
	/** 银行卡兑换服务 */
	public static final String YIN_HANG = MAIN+"android.php/Convent/and_bank_record";
	/** 分享内容的获取 */
	public static final String SHARE_CONTENT = MAIN+"android.php/Makemoney/and_share_content";
	/** 兑换界面图片的获取 */
	public static final String SHARE_IMAGE = MAIN+"android.php/Homepage/share_image";
	/** 接口版本JSON信息获取 */
	public static final String VERSION = MAIN+"android.php/Yscm/and_version";
	/** 设置获得金币 */
	public static final String SET_GOLD = MAIN+"android.php/Makemoney/and_uplode_record";
	/** 获得网络ad图片集合 */
	public static final String GET_VIEWPAGER = MAIN+"android.php/Homepage/roll_image";
	/** 意见反馈 */
	public static final String FEED_BACK = MAIN+"android.php/Set/and_opinion";
	/** 分享上传服务器 */
	public static final String SHARE_RECORD = MAIN+"android.php/Makemoney/and_share_record";
	/** 获取吃喝玩乐链接 */
	public static final String SHOPING_WEB = MAIN+"android.php/Homepage/and_chwl";
	/** 获取热门活动链接 */
	public static final String HOT_ACTIVITY = MAIN+"android.php/Homepage/and_rmhd";
	/** 获取嵌入积分墙列表 */
	public static final String MAKE_MONEY = MAIN+"android.php/Makemoney/integral_wall";
	/** 上传当前位置信息 */
	public static final String LOCATION = MAIN+"android.php/Survey/and_geography";
	/** 获取锁屏广告数据源 */
	public static final String LEFT_LOCK = MAIN+"android.php/Lock/left";
	/** 获取解锁是否得金币 */
	public static final String RIGHT_LOCK = MAIN+"android.php/Lock/right";
	/** 上传滑动记录*/
	public static final String LOCK_GOLD = MAIN+"android.php/Lock/lock_record";
	/** 是否加载锁屏新数据*/
	public static final String IS_CACHE = MAIN+"android.php/Lock/mem";
	/** 默认锁屏界面图片*/
	public static final String LOCK_IMAGE1 = MAIN+"Public/Images/jbimage/1.jpg";
	public static final String LOCK_IMAGE2 = MAIN+"Public/Images/jbimage/2.jpg";
	public static final String LOCK_IMAGE3 = MAIN+"Public/Images/jbimage/3.jpg";
	public static final String LOCK_IMAGE4 = MAIN+"Public/Images/jbimage/4.jpg";
	/** 判断以前是否注册过*/
	public static final String IS_LOGIN = MAIN+"android.php/Yscm/detect";
	/** 获取关注数据源*/
	public static final String ATTEN = MAIN+"android.php/Attention/index";
	/** 获取关注商家的次数*/
	public static final String ATTEN_COUNT = MAIN+"android.php/Attention/atten_count";
	/** 获取用户当前的消息个数*/
	public static final String MESSGE = MAIN+"android.php/Yscm/mess_update";
	/** 获取用户是否需要提交*/
	public static final String IS_SUBMIT = MAIN+"android.php/Survey/and_news_repeat";
	/** 提交用户的问卷调查结果*/
	public static final String SUBMIT_QUS = MAIN+"android.php/Survey/and_news";
	/** 提交用户的微信和QQ*/
	public static final String SUBMIT_QQ = MAIN+"android.php/Attention/and_weixin_qq";
	/**	分享字段拼接*/
	public static final String SHARE_1 = MAIN+"a/";
	public static final String SHARE_2 = "/b/";
	public static final String SHARE_4 = "/e/";
	public static final String SHARE_5 = "/c/android/d/";
	public static final String SHARE_6 = "a9fe6fd5";
	public static final String SHARE_7 = "/f/";
	public static final String SHARE_8 = ".html";
	
	/** 记录用户分享次数*/
	public static final String SHARE_COUNT = MAIN+"android.php/Attention/and_atten_record";
}
