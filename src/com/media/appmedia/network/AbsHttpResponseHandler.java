package com.media.appmedia.network;

import android.content.Context;

import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;

public class AbsHttpResponseHandler extends RequestCallBack<String> {

	private Context mContext;

	public AbsHttpResponseHandler(Context ctx) {
		this.mContext = ctx;
	}

	@Override
	public void onStart() {
		ProgressDialogUtil.showDialog(mContext);
		super.onStart();
	}

	@Override
	public void onFailure(HttpException arg0, String arg1) {
		ProgressDialogUtil.closeDialog();
	}

	@Override
	public void onSuccess(ResponseInfo<String> arg0) {
		ProgressDialogUtil.closeDialog();
	}

}
