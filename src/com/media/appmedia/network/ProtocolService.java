package com.media.appmedia.network;

import android.content.Context;

import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.media.appmedia.base.BaseApplication;
import com.media.appmedia.util.MethodUtils;

/**
 * 
 * 作者: Rose 
 * 业务名:联网业务 
 * 功能说明: 联网下载数据封装类 
 * 编写日期: 2014-12-4
 * 
 */
public class ProtocolService {

	private static Context context = BaseApplication.getApplication()
			.getApplicationContext();

	public static void initConfig(Context mContext) {
		context = mContext;
	}

	/**
	 * 获取手机验证码
	 * 
	 * @param phoneNumber
	 *            手机号
	 * @param 验证码发送类型
	 * @param RequestCallBack
	 *            <String>
	 */
	public static void getVerifyPhoneCode(String phoneNumber, String type,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.USER_VERFIYCODE;
		RequestParams params = new RequestParams();
		params.addBodyParameter("telephone", phoneNumber);
		params.addBodyParameter("biaoshifu", type);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 注册
	 * 
	 * @param integer
	 *            $phone 手机号码
	 * @param integer
	 *            $code 验证码 1111可用
	 * @param integer
	 *            $userMac 用户mac
	 * @param integer
	 *            $shId 商户id=3
	 * @param integer
	 *            $routeId 路由器id=4
	 */
	public static void register(String cellphone, String verfiycode,
			String userMac, String shId, String source,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.REGIST_REGISTER;
		RequestParams params = new RequestParams();
		params.addBodyParameter("telephone", cellphone);// 手机号
		params.addBodyParameter("rand", verfiycode);// 验证码
		params.addBodyParameter("role", userMac);// 用户名
		params.addBodyParameter("biaoshifu", shId);// 手机标示符
		params.addBodyParameter("source", source);// 来源途径
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 我的信息
	 * 
	 * @param phoneNumber
	 *            手机号
	 * @param RequestCallBack
	 *            <String>
	 */
	public static void getVerifyMy(String phoneNumber, String biaoshifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.My_URL;
		RequestParams params = new RequestParams();
		params.addBodyParameter("telephone", phoneNumber);
		params.addBodyParameter("biaoshifu", biaoshifu);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：首页等级和金币信息获取
	 * 
	 * @param biaoshifu
	 * @param handler
	 */
	public static void getVerifyMy(String biaoshifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.MY_GRADE;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaoshifu);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：是否可以签到
	 * 
	 * @param biaoshifu
	 * @param handler
	 */
	public static void getIsSign(String biaoshifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.IS_SIGN;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaoshifu);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：用于发送抽奖结果至服务器
	 * 
	 * @param biaoshifu
	 * @param RMB
	 * @param handler
	 */
	public static void putRMB(String biaoshifu, String RMB,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.POST_RMB;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaoshifu);
		params.addBodyParameter("sign_record_money", RMB);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 我的信息
	 * 
	 * @param integer
	 *            $phone 手机号码
	 */
	public static void my(String Mob_id, RequestCallBack<String> handler) {
		String method = ProtocolConst.REGIST_REGISTER;
		RequestParams params = new RequestParams();
		params.addBodyParameter("telephone", Mob_id);// 手机号
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：请求得到 获取积分详情数据集合
	 * 
	 * @param biaozhifu
	 * @param handler
	 */
	public static void obtainPoints(String biaozhifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.GET_POINTS;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：请求得到积分消费详情数据集合
	 * 
	 * @param biaozhifu
	 * @param handler
	 */
	public static void putPoints(String biaozhifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.PUT_POINTS;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：获取积分数据(当前积分和历史最高积分)
	 * 
	 * @param biaozhifu
	 * @param handler
	 */
	public static void getJiFen(String biaozhifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.JIFEN;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：获取推广好友信息数据
	 * 
	 * @param biaozhifu
	 * @param handler
	 */
	public static void getFriends(String biaozhifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.FRIENDS;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：获取推广好友数量数据
	 * 
	 * @param biaozhifu
	 * @param handler
	 */
	public static void getCount(String biaozhifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.FRIENDS_COUNT;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：获取我的消息数据
	 * 
	 * @param biaozhifu
	 * @param handler
	 */
	public static void getMyMessges(String biaozhifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.MY_MESSGES;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：获取系统消息
	 * 
	 * @param biaozhifu
	 * @param handler
	 */
	public static void getSystemMessges(String biaozhifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.SYSTEM_MESSGES;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：qq兑换功能
	 * 
	 * @param biaozhifu
	 * @param QQ
	 * @param money
	 * @param handler
	 */
	public static void putQQpay(String biaozhifu, String QQ, String money,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.QQ_PAY;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		params.addBodyParameter("qbi_record_money", money);
		params.addBodyParameter("qbi_record_qq", QQ);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：手机充值
	 * 
	 * @param biaozhifu
	 * @param phone
	 * @param money
	 * @param handler
	 */
	public static void putPhonePay(String biaozhifu, String phone,
			String money, RequestCallBack<String> handler) {
		String method = ProtocolConst.PHONE_PAY;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		params.addBodyParameter("bill_record_money", money);
		params.addBodyParameter("bill_record_phone", phone);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：支付寶兌換功能
	 * 
	 * @param biaozhifu
	 * @param aliPay
	 * @param money
	 * @param userName
	 * @param handler
	 */
	public static void putAliPay(String biaozhifu, String aliPay, String money,
			String userName, RequestCallBack<String> handler) {
		String method = ProtocolConst.ALI_PAY;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		params.addBodyParameter("alipay_record_money", money);
		params.addBodyParameter("alipay_record_num", aliPay);
		params.addBodyParameter("alipay_record_name", userName);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：财付通兑换实现功能
	 * 
	 * @param biaozhifu
	 * @param tenPay
	 * @param money
	 * @param userName
	 * @param handler
	 */
	public static void putTenPay(String biaozhifu, String tenPay, String money,
			String userName, RequestCallBack<String> handler) {
		String method = ProtocolConst.TEN_PAY;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		params.addBodyParameter("tenpay_record_money", money);
		params.addBodyParameter("tenpay_record_num", tenPay);
		params.addBodyParameter("tenpay_record_name", userName);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：银行卡兑换实现功能
	 * 
	 * @param biaozhifu
	 * @param tenPay
	 * @param money
	 * @param userName
	 * @param handler
	 */
	public static void putYinhangka(String biaozhifu, String tenPay, String money,
			String userName, RequestCallBack<String> handler) {
		String method = ProtocolConst.YIN_HANG;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		params.addBodyParameter("bank_record_money", money);
		params.addBodyParameter("bank_record_num", tenPay);
		params.addBodyParameter("bank_record_name", userName);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：网络获取分享内容
	 * 
	 * @param biaozhifu
	 * @param handler
	 */
	public static void getShareContent(String biaozhifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.SHARE_CONTENT;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		params.addBodyParameter("share_record_content", "3");
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：得到兑换界面一张图片
	 * 
	 * @param biaozhifu
	 * @param handler
	 */
	public static void getImageView(String biaozhifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.SHARE_IMAGE;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：获取当前版本号
	 * 
	 * @param biaozhifu
	 * @param handler
	 */
	public static void getViersion(String biaozhifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.VERSION;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：上传金币到服务器
	 * 
	 * @param biaozhifu
	 * @param name
	 * @param content
	 * @param numb
	 * @param handler
	 */
	public static void setGolds(String biaozhifu, String name, String content,
			int numb, RequestCallBack<String> handler) {
		String method = ProtocolConst.SET_GOLD;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		params.addBodyParameter("uplode_record_name", name);
		params.addBodyParameter("uplode_record_software", content);
		params.addBodyParameter("uplode_record_money", numb + "");
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：获取服务器轮播广告展示图片集合
	 * 
	 * @param biaozhifu
	 * @param handler
	 */
	public static void setViewPager(String biaozhifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.GET_VIEWPAGER;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}

	/**
	 * 
	 * 方法说明：意见反馈到服务器
	 * 
	 * @param biaozhifu
	 *            标识符
	 * @param wall
	 *            积分墙名称
	 * @param soft
	 *            任务名称
	 * @param integral
	 *            任务金币
	 * @param rentime
	 *            做任务时间
	 * @param content
	 *            简单描述
	 * @param time
	 *            入库时间
	 * @param handler
	 */
	public static void putFeedBack(String biaozhifu, String wall, String soft,
			String integral, String content, RequestCallBack<String> handler) {
		String method = ProtocolConst.FEED_BACK;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		params.addBodyParameter("and_opinion_wall", wall);
		params.addBodyParameter("and_opinion_software", soft);
		params.addBodyParameter("and_opinion_integral", integral);
		// params.addBodyParameter("iph_opinion_rentime", rentime);
		params.addBodyParameter("and_opinion_content", content);
		// params.addBodyParameter("iph_opinion_time", time);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：分享传递给服务器
	 *
	 * @param biaozhifu
	 * @param handler
	 */
	public static void putShareGold(String biaozhifu,String share_record_content, String share_record_type,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.SHARE_RECORD;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		params.addBodyParameter("share_record_content", share_record_content);
		params.addBodyParameter("share_record_type", share_record_type);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：获取吃喝玩乐城网络链接数据
	 *
	 * @param biaozhifu
	 * @param handler
	 */
	public static void getShoping(String biaozhifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.SHOPING_WEB;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：获取 热门活动的网络数据
	 *
	 * @param biaozhifu
	 * @param handler
	 */
	public static void getHot(String biaozhifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.HOT_ACTIVITY;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：获取积分墙显示列表
	 *
	 * @param biaozhifu
	 * @param handler
	 */
	public static void getMakes(String biaozhifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.MAKE_MONEY;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：上传用户当前位置信息
	 *
	 * @param biaozhifu
	 * @param province
	 * @param city
	 * @param district
	 * @param street
	 * @param handler
	 */
	public static void putLocation(String biaozhifu,
			String province,String city,String district,String street,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.LOCATION;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		params.addBodyParameter("dili_weizhi_province", province);
		params.addBodyParameter("dili_weizhi_city", city);
		params.addBodyParameter("dili_weizhi_district", district);
		params.addBodyParameter("dili_weizhi_street", street);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：获取锁屏界面广告数据
	 *
	 * @param biaozhifu
	 * @param handler
	 */
	public static void getLockLeft(String biaozhifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.LEFT_LOCK;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：获取解锁可得金币数量
	 *
	 * @param biaozhifu
	 * @param handler
	 */
	public static void getLockRight(String biaozhifu,String lock_image_id,String lock_image_photo,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.RIGHT_LOCK;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		params.addBodyParameter("lock_image_id", lock_image_id);
		params.addBodyParameter("lock_image_photo", lock_image_photo);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：解锁后上传，由服务器判定是否增加积分
	 *
	 * @param biaozhifu
	 * @param id
	 * @param direct
	 * @param handler
	 */
	public static void putGold(String biaozhifu,String id,String direct,String lock_record_photo,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.LOCK_GOLD;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		params.addBodyParameter("lock_record_id", id);
		params.addBodyParameter("lock_record_direct", direct);
		params.addBodyParameter("lock_record_photo", lock_record_photo);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：获取是否需要重新缓存锁屏数据
	 *
	 * @param biaozhifu
	 * @param handler
	 */
	public static void isCache(String biaozhifu ,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.IS_CACHE;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：是否已经注册过
	 *
	 * @param biaozhifu
	 * @param handler
	 */
	public static void isLogin(String biaozhifu ,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.IS_LOGIN;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：获取分享关注的广告数据源
	 *
	 * @param biaozhifu
	 * @param handler
	 */
	public static void getAtten(String biaozhifu ,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.ATTEN;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：获取关注商家的剩余次数
	 *
	 * @param biaozhifu
	 * @param atten_name
	 * @param handler
	 */
	public static void getAttenCount(String biaozhifu ,String atten_name,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.ATTEN_COUNT;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		params.addBodyParameter("atten_record_name", atten_name);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：获取用户当前的消息条数
	 *
	 * @param biaozhifu
	 * @param handler
	 */
	public static void getMessge(String biaozhifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.MESSGE;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：获取用户是否需要进行调查问卷
	 *
	 * @param biaozhifu
	 * @param handler
	 */
	public static void getIsSub(String biaozhifu,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.IS_SUBMIT;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：提交问卷调查结果
	 *
	 * @param biaozhifu
	 * @param handler
	 */
	public static void submitQestion(String biaozhifu,String question_search_age,
			String question_search_xinzi,String question_search_aihao,
			String question_search_sex,String question_search_culture,
			String question_search_trade,String question_search_sjlxing,
			String question_search_sjbzhi,String question_search_opinion,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.SUBMIT_QUS;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		params.addBodyParameter("question_search_age", question_search_age);
		params.addBodyParameter("question_search_xinzi", question_search_xinzi);
		params.addBodyParameter("question_search_aihao", question_search_aihao);
		params.addBodyParameter("question_search_sex", question_search_sex);
		params.addBodyParameter("question_search_culture", question_search_culture);
		params.addBodyParameter("question_search_trade", question_search_trade);
		params.addBodyParameter("question_search_sjlxing", question_search_sjlxing);
		params.addBodyParameter("question_search_sjbzhi", question_search_sjbzhi);
		params.addBodyParameter("question_search_opinion", question_search_opinion);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：提交用户的微信号和QQ号
	 *
	 * @param biaozhifu
	 * @param weix
	 * @param QQ
	 * @param handler
	 */
	public static void subQQ(String biaozhifu,String weix,String QQ,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.SUBMIT_QQ;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		params.addBodyParameter("weixin", weix);
		params.addBodyParameter("qq", QQ);
		VtionHttpClient.post(context, method, params, handler);
	}
	/**
	 * 
	 * 方法说明：上传记录用户当前分享次数
	 *
	 * @param biaozhifu
	 * @param atten_record_name
	 * @param atten_record_url
	 * @param atten_record_type
	 * @param handler
	 */
	public static void shareCount(String biaozhifu,String atten_record_name,
			String atten_record_url,String atten_record_type,
			RequestCallBack<String> handler) {
		String method = ProtocolConst.SHARE_COUNT;
		RequestParams params = new RequestParams();
		params.addBodyParameter("biaoshifu", biaozhifu);
		params.addBodyParameter("atten_record_name", atten_record_name);
		params.addBodyParameter("atten_record_url", atten_record_url);
		params.addBodyParameter("atten_record_type", atten_record_type);
		VtionHttpClient.post(context, method, params, handler);
	}
}
