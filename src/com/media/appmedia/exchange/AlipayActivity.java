package com.media.appmedia.exchange;

import org.json.JSONException;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.media.appmedia.R;
import com.media.appmedia.base.BaseActivity;
import com.media.appmedia.network.ProtocolService;
import com.media.appmedia.util.MethodUtils;
import com.media.appmedia.util.PhoneInfoUtils;
import com.media.appmedia.util.ToastUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

/**
 * 
 * <pre>
 * 业务名:
 * 功能说明: 支付宝兑换功能
 * 编写日期:	2014-11-13
 * 作者:	 Rose
 * 
 * 历史记录
 * 1、修改日期：
 *    修改人：
 *    修改内容：
 * </pre>
 */
public class AlipayActivity extends BaseActivity implements OnClickListener {
	@ViewInject(R.id.Relanumberzhi)
	private RadioGroup radioGroup;
	@ViewInject(R.id.edittextzhi)
	private EditText ali1;
	@ViewInject(R.id.edittextcai2zhi)
	private EditText ali2;
	@ViewInject(R.id.edittextcai3zhi)
	private EditText userName;
	private ImageButton mImageButtonreturn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去掉标题栏
		setContentView(R.layout.alipay_layout);
		initView();
		ViewUtils.inject(this);
		// radioButton点击事件处理
		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				String aliStr1 = ali1.getText().toString().trim();
				String aliStr2 = ali2.getText().toString().trim();
				String userNameStr = userName.getText().toString().trim();
				if (MethodUtils.isEmpty(aliStr1)
						|| MethodUtils.isEmpty(aliStr2)
						|| !aliStr1.equals(aliStr2)
						|| MethodUtils.isEmpty(userNameStr)) {
					ToastUtils.show(getApplicationContext(), "请核对您输入账号和用户名");
					backButton(radioGroup);
					return;
				}
				int money = 0;
				if (checkedId == radioGroup.getChildAt(0).getId()) {
					money = 10;
				} else if (checkedId == radioGroup.getChildAt(1).getId()) {
					money = 30;
				} else if (checkedId == radioGroup.getChildAt(2).getId()) {
					money = 50;
				}
				putAliPay(PhoneInfoUtils.getIMEI(getApplicationContext()),
						aliStr1, money + "", userNameStr);

			}
		});
	}

	private void initView() {
		mImageButtonreturn = (ImageButton) findViewById(R.id.alipayreturn);
		mImageButtonreturn.setOnClickListener(this);
	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.alipayreturn:
			finish();
			break;

		default:
			break;
		}
	}

	private void putAliPay(String biaoshifu, String alipay, String money,
			String userName) {
		ProtocolService.putAliPay(biaoshifu, alipay, money, userName,
				new RequestCallBack<String>() {
					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							org.json.JSONObject obj = new org.json.JSONObject(
									arg0.result);
							if (obj.getInt("message") == 1) {
								AlertDialog.Builder alertbBuilder = new AlertDialog.Builder(
										AlipayActivity.this);
								alertbBuilder
										.setTitle("提示")
										.setMessage("恭喜您提现成功\n分享立即获得20金币")
										.setPositiveButton(
												"分享到朋友圈",
												new DialogInterface.OnClickListener() {
													@Override
													public void onClick(
															DialogInterface dialog,
															int which) {
														dialog.cancel();
														initSocialSDK("太高兴了，我在升官发财应用里成功提现大量的现金。这下有钱我终于可以挥金如土，穷奢极欲，一掷千金，任性的造了！"
																,2+"");
														openShare();
														AlipayActivity.this
																.finish();
													}
												}).create();
								alertbBuilder.show();
							} else if(obj.getInt("message") == 0){
								int xType = obj.getInt("error");
								String resultReson = "";
								switch (xType) {
								case 1:
									resultReson = "金币不足！";
									break;
								case 2:
									resultReson = "一个月只能兑换一次！";
									break;
								case 3:
									resultReson = "系统错误 ！";
									break;
								}
								AlertDialog.Builder alertbBuilder = new AlertDialog.Builder(
										AlipayActivity.this);
								alertbBuilder
										.setTitle("提示")
										.setMessage("充值失败！\n"+resultReson)
										.setPositiveButton(
												"确定",
												new DialogInterface.OnClickListener() {
													@Override
													public void onClick(
															DialogInterface dialog,
															int which) {
														dialog.cancel();
														AlipayActivity.this
																.finish();
													}
												}).create();
								alertbBuilder.show();
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						ToastUtils.show(getApplicationContext(),
								R.string.phone_verify_send_fail);
					}
				});
	}

	/**
	 * 
	 * 方法说明：将传入的RadioGroup中的内容归为未选中状态(后期移动到baseActivity中)
	 * 
	 * @param radioGroup
	 */
	private void backButton(RadioGroup radioGroup) {
		for (int i = 0; i < radioGroup.getChildCount(); i++) {
			RadioButton button = (RadioButton) radioGroup.getChildAt(i);
			button.setChecked(false);
		}
	}

}
