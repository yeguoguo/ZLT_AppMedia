package com.media.appmedia.exchange;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import com.media.appmedia.R;
import com.media.appmedia.base.BaseActivity;

/**
 * 
 * <pre>
 * 业务名:
 * 功能说明: 礼品兑换功能
 * 编写日期:	2014-11-13
 * 作者:	 Rose
 * 
 * 历史记录
 * 1、修改日期：
 *    修改人：
 *    修改内容：
 * </pre>
 */
public class GiftActivity extends BaseActivity implements OnClickListener {

	private ImageButton mImageButtongift;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去掉标题栏
		setContentView(R.layout.gift_layout);
		initView();
	}

	private void initView() {
		mImageButtongift = (ImageButton) findViewById(R.id.giftreturn);
		mImageButtongift.setOnClickListener(this);
	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.giftreturn:
			finish();
			break;

		default:
			break;
		}
	}
}
