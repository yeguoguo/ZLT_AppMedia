package com.media.appmedia.slidingmenu;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.media.appmedia.R;
import com.media.appmedia.alipays.PayResult;
import com.media.appmedia.alipays.SignUtils;
import com.media.appmedia.util.PhoneInfoUtils;
import com.media.appmedia.util.ToastUtils;

/**
 * 
 * 作者: Rose 
 * 业务名: 金币充值 
 * 功能说明: 用户充值金币(支付宝) 
 * 编写日期: 2015-1-26
 * 
 */
public class AccountActivity extends Activity {
	//商户PID
	public static final String PARTNER = "2088811029886326";
	//商户收款账号
	public static final String SELLER = "rongshangshoukuan@aliyun.com";
	//商户私钥，pkcs8格式
	public static final String RSA_PRIVATE = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAKRyBh1AewWBRtzFGberh5+2/mmlO/vWVooRIRj/ByGZl7Mbyxs6/O4WdSzUf272Sm6ecP8YxBKWpaBb/F3SbaNIG/pI3toj/4F7Jk8V1qFKuXiNXdquSW9B60fbQuTDne5qIREW0syAfgaMWEGfFvcC3mTxsIqtu4VbS3TD0Oo3AgMBAAECgYA/dIRpez09dZIrlJ8E4dq2xb1MdOc9mYrRQiTcaWSOEZ+ZDKuGOW/qS54xi1oVLlKwdtExjpPyfFEzHOu4JIALeBnxRwkBPcyDg+Cv19PAka0sJ6GUMi5+WuZxrfE1vQQc5Q9C4Q1r64gflv+yNtl55dyXTlvD3U7RnPhDBhp6sQJBANp3BH7ztrctPSRJDAaJsUAYnuoG7n6UsH+KRFUPclhgQgfhhChf0n80NoMMuZTpvNOtHhJNLgL4SXpb0/QtkR8CQQDAswHI4hNU33Q6J0tLb2RrypGTFbEfBslVdS7Yvq5/GowrtD0bzAnwZxt4tKVxBOZfbwPjrfdQpq2C3+Wp9IvpAkAcnv8dyrwjX2/lMxS6PovUQ+V9746LUtuUuf6dzjpCzcmY0rjHuVYQiDykCqS9l6m/aazI5QekJ/coaqNoGBgtAkATzxMC5IKaOfJHvBxYX9YR4pv+Y792cgsmVsp/C3MCq+b24ytY/SuUY/UF/oP3T0qjStU6hXqj9s5lYsDILNRpAkBtIMcXaVKUPrksMuFeKmqHJjLal37A/esYTnTnMCBHOe8tzytZKTRAD1XlhaPOJ/jOBp2tosPRRQdoMQpGPqin";
	//支付宝公钥
	public static final String RSA_PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCkcgYdQHsFgUbcxRm3q4eftv5ppTv71laKESEY/wchmZezG8sbOvzuFnUs1H9u9kpunnD/GMQSlqWgW/xd0m2jSBv6SN7aI/+BeyZPFdahSrl4jV3arklvQetH20Lkw53uaiERFtLMgH4GjFhBnxb3At5k8bCKrbuFW0t0w9DqNwIDAQAB";

	private static final int SDK_PAY_FLAG = 1;

	private static final int SDK_CHECK_FLAG = 2;
	@ViewInject(R.id.edittext_count)
	private EditText editText;

	public static final String TAG = "alipay-sdk";
	/**
	 * 支付宝支付回调结果处理
	 */
	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case SDK_PAY_FLAG: {
				PayResult payResult = new PayResult((String) msg.obj);
				// 支付宝返回此次支付结果及加签，建议对支付宝签名信息拿签约时支付宝提供的公钥做验签
				String resultInfo = payResult.getResult();
				String resultStatus = payResult.getResultStatus();
				// 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
				if (TextUtils.equals(resultStatus, "9000")) {
					Toast.makeText(AccountActivity.this, "支付成功",
							Toast.LENGTH_SHORT).show();
				} else {
					// 判断resultStatus 为非“9000”则代表可能支付失败
					// “8000”代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
					if (TextUtils.equals(resultStatus, "8000")) {
						Toast.makeText(AccountActivity.this, "支付结果确认中",
								Toast.LENGTH_SHORT).show();
					} else {
						// 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
						Toast.makeText(AccountActivity.this, "支付失败",
								Toast.LENGTH_SHORT).show();
					}
				}
				break;
			}
			case SDK_CHECK_FLAG: {
				Toast.makeText(AccountActivity.this, "检查结果为：" + msg.obj,
						Toast.LENGTH_SHORT).show();
				break;
			}
			default:
				break;
			}
		};
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去掉标题栏
		setContentView(R.layout.account_layout);
		ViewUtils.inject(this);

	}
	
	/**
	 * call alipay sdk pay. 调用SDK支付
	 * 
	 */
	public void pay(int count) {
		// 订单
		String orderInfo = getOrderInfo("赚乐淘-"+count+"金币-", 
				"赚乐淘金币,"+PhoneInfoUtils.getIMEI(getApplicationContext())
				+",android", ((double)count)/100+"");
		// 对订单做RSA 签名
		String sign = sign(orderInfo);
		try {
			// 仅需对sign 做URL编码
			sign = URLEncoder.encode(sign, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		// 完整的符合支付宝参数规范的订单信息
		final String payInfo = orderInfo + "&sign=\"" + sign + "\"&"
				+ getSignType();
		Runnable payRunnable = new Runnable() {
			@Override
			public void run() {
				// 构造PayTask 对象
				PayTask alipay = new PayTask(AccountActivity.this);
				// 调用支付接口，获取支付结果
				String result = alipay.pay(payInfo);

				Message msg = new Message();
				msg.what = SDK_PAY_FLAG;
				msg.obj = result;
				mHandler.sendMessage(msg);
			}
		};
		// 必须异步调用
		Thread payThread = new Thread(payRunnable);
		payThread.start();
	}

	@OnClick({ R.id.come_back, R.id.btn_add_gold })
	public void back(View v) {
		switch (v.getId()) {
		case R.id.come_back:
			AccountActivity.this.finish();
			break;
		case R.id.btn_add_gold:
			// 进行金币充值了
			pay(parseInt(editText.getText().toString().trim()));
			break;
		}
	}
	
	/**
	 * check whether the device has authentication alipay account.
	 * 查询终端设备是否存在支付宝认证账户
	 * 
	 */
//	private void addGold(){
//		Runnable checkRunnable = new Runnable() {
//
//			@Override
//			public void run() {
//				// 构造PayTask 对象
//				PayTask payTask = new PayTask(AccountActivity.this);
//				// 调用查询接口，获取查询结果
//				boolean isExist = payTask.checkAccountIfExist();
//
//				Message msg = new Message();
//				msg.what = SDK_CHECK_FLAG;
//				msg.obj = isExist;
//				mHandler.sendMessage(msg);
//			}
//		};
//
//		Thread checkThread = new Thread(checkRunnable);
//		checkThread.start();
//	}

	private int parseInt(String count) {
		int x;
		try {
			x = Integer.parseInt(count);
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), "你输入的金币格式有误,按1金币进行充值！",Toast.LENGTH_SHORT).show();
			x = 1;
		}
		if (x < 1) {
			Toast.makeText(getApplicationContext(), "你输入的金币数量不足1金币,按1金币进行充值！",Toast.LENGTH_SHORT).show();
			x = 1;
		}
		return x;
	}

	/**
	 * get the sdk version. 获取SDK版本号
	 * 
	 */
	public void getSDKVersion() {
		PayTask payTask = new PayTask(this);
		String version = payTask.getVersion();
		Toast.makeText(this, version, Toast.LENGTH_SHORT).show();
	}

	/**
	 * create the order info. 创建订单信息
	 * 
	 */
	public String getOrderInfo(String subject, String body, String price) {
		// 签约合作者身份ID
		String orderInfo = "partner=" + "\"" + PARTNER + "\"";
		// 签约卖家支付宝账号
		orderInfo += "&seller_id=" + "\"" + SELLER + "\"";
		// 商户网站唯一订单号
		orderInfo += "&out_trade_no=" + "\"" + getOutTradeNo() + "\"";
		// 商品名称
		orderInfo += "&subject=" + "\"" + subject + "\"";
		// 商品详情
		orderInfo += "&body=" + "\"" + body + "\"";
		// 商品金额
		orderInfo += "&total_fee=" + "\"" + price + "\"";
		// 服务器异步通知页面路径
		orderInfo += "&notify_url=" + "\"" + "http://www.sgfc88.com/alipay/notify_url.php"
				+ "\"";
		// 服务接口名称， 固定值
		orderInfo += "&service=\"mobile.securitypay.pay\"";
		// 支付类型， 固定值
		orderInfo += "&payment_type=\"1\"";
		// 参数编码， 固定值
		orderInfo += "&_input_charset=\"utf-8\"";
		// 设置未付款交易的超时时间
		// 默认30分钟，一旦超时，该笔交易就会自动被关闭。
		// 取值范围：1m～15d。
		// m-分钟，h-小时，d-天，1c-当天（无论交易何时创建，都在0点关闭）。
		// 该参数数值不接受小数点，如1.5h，可转换为90m。
		orderInfo += "&it_b_pay=\"30m\"";
		// extern_token为经过快登授权获取到的alipay_open_id,带上此参数用户将使用授权的账户进行支付
		// orderInfo += "&extern_token=" + "\"" + extern_token + "\"";
		// 支付宝处理完请求后，当前页面跳转到商户指定页面的路径，可空
		orderInfo += "&return_url=\"m.alipay.com\"";
		// 调用银行卡支付，需配置此参数，参与签名， 固定值 （需要签约《无线银行卡快捷支付》才能使用）
		// orderInfo += "&paymethod=\"expressGateway\"";
		return orderInfo;
	}

	/**
	 * get the out_trade_no for an order. 生成商户订单号，该值在商户端应保持唯一（可自定义格式规范）
	 * 
	 */
	public String getOutTradeNo() {
		SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss",
				Locale.getDefault());
		Date date = new Date();
		String key = format.format(date);

		Random r = new Random();
		key = key + r.nextInt();
		key = key.substring(0, 15);
		return key;
	}

	/**
	 * sign the order info. 对订单信息进行签名
	 * 
	 * @param content
	 *            待签名订单信息
	 */
	public String sign(String content) {
		return SignUtils.sign(content, RSA_PRIVATE);
	}

	/**
	 * get the sign type we use. 获取签名方式
	 * 
	 */
	public String getSignType() {
		return "sign_type=\"RSA\"";
	}
}
