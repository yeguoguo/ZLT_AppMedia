package com.media.appmedia.slidingmenu;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.media.appmedia.R;
import com.media.appmedia.adapter.MyFriendsAdapter;
import com.media.appmedia.entity.MyFriends;
import com.media.appmedia.network.ProtocolService;
import com.media.appmedia.util.PhoneInfoUtils;
import com.media.appmedia.util.ToastUtils;

/**
 * 
 * <pre>
 * 业务名:
 * 功能说明: 推广好友
 * 编写日期:	2014-12-2
 * 作者:	 Rose
 * 
 * 历史记录
 * 1、修改日期：
 *    修改人：
 *    修改内容：
 * </pre>
 */
public class GeneralizeActivity extends Activity {
	@ViewInject(R.id.friends)
	private ListView friendList;
	@ViewInject(R.id.finend_all)
	private TextView text_all;
	@ViewInject(R.id.finend_month)
	private TextView text_month;
	@ViewInject(R.id.finend_day)
	private TextView text_day;
	private ArrayList<MyFriends> list = new ArrayList<MyFriends>();
	private View pop_view;
	int n = 0;
	private PopupWindow mPopupWindow;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.invitation_layout);
		ViewUtils.inject(this);
		getListView(PhoneInfoUtils.getIMEI(getApplicationContext()));
		getFriendCount();
		initPopup();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (hasFocus && mPopupWindow != null && n == 0) {
			mPopupWindow.showAtLocation(pop_view, Gravity.BOTTOM, 0, 0);
			n++;
		}
	}

	private void initPopup() {
		// LayoutInflater inflater =
		// (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		pop_view = LayoutInflater.from(GeneralizeActivity.this).inflate(
				R.layout.popu_tuiguang_layout, null);
		mPopupWindow = new PopupWindow(pop_view, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT, true);
//		mPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
		mPopupWindow.setFocusable(true);
		mPopupWindow.setAnimationStyle(R.style.anim_menu_popup);
		mPopupWindow.setOutsideTouchable(false);
		ViewUtils.inject(this , pop_view);
	}

	@OnClick({ R.id.close_pop })
	public void closePop(View v) {
		if (mPopupWindow != null) {
			mPopupWindow.dismiss();
		}
	}

	private void getListView(String biaoshifu) {
		ProtocolService.getFriends(biaoshifu, new RequestCallBack<String>() {
			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				try {
					org.json.JSONObject obj = new org.json.JSONObject(
							arg0.result);
					if (obj.getInt("message") == 1) {
						JSONArray array = new JSONArray(obj.getString("0"));
						for (int i = 0; i < array.length(); i++) {
							org.json.JSONObject Obj = array.getJSONObject(i);
							MyFriends bean = new MyFriends();
							bean.setFriend_name(Obj.getString("friend_name"));
							bean.setGet_integral_to_fiend(Obj
									.getString("get_integral_to_fiend"));
							list.add(bean);
						}
						MyFriendsAdapter adapter = new MyFriendsAdapter(list,
								getApplicationContext());
						friendList.setAdapter(adapter);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(HttpException arg0, String arg1) {
				ToastUtils.show(getApplicationContext(),
						R.string.phone_verify_send_fail);
			}
		});
	}

	private void getFriendCount() {
		ProtocolService.getCount(
				PhoneInfoUtils.getIMEI(getApplicationContext()),
				new RequestCallBack<String>() {

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							JSONObject obj = new JSONObject(arg0.result);
							if (obj.getInt("message") == 1) {
								text_all.setText(obj.getString("total"));
								text_day.setText(obj.getString("today"));
								text_month.setText(obj.getString("month"));
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						ToastUtils.show(getApplicationContext(),
								R.string.phone_verify_send_fail);
					}
				});
	}

	@OnClick({ R.id.come_back, R.id.come_back_text })
	public void back(View v) {
		GeneralizeActivity.this.finish();
	}

}
