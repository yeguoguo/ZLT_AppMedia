package com.media.appmedia.slidingmenu;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.media.appmedia.R;
import com.media.appmedia.activity.AdWebView;
import com.media.appmedia.adapter.GetJiFenAdapter;
import com.media.appmedia.adapter.PutJiFenAdapter;
import com.media.appmedia.base.BaseApplication;
import com.media.appmedia.base.MeBaseAdapter;
import com.media.appmedia.entity.GetJiFenBean;
import com.media.appmedia.entity.JSON;
import com.media.appmedia.entity.PutJiFenBean;
import com.media.appmedia.entity.UserGradeInfo;
import com.media.appmedia.network.ProtocolService;
import com.media.appmedia.util.DateUtils;
import com.media.appmedia.util.MethodUtils;
import com.media.appmedia.util.PhoneInfoUtils;
import com.media.appmedia.util.ToastUtils;

/**
 * 
 * <pre>
 * 业务名:
 * 功能说明: 侧滑菜单里的积分明细
 * 编写日期:	2014-10-23
 * 作者:	 季佳满
 * 
 * 历史记录
 * 1、修改日期：
 *    修改人：
 *    修改内容：
 * </pre>
 */
public class IntegralActivity extends Activity {
	@ViewInject(R.id.textViewAccumulativejifen)
	private TextView history;
	@ViewInject(R.id.textViewAccountjifen)
	private TextView current;
	@ViewInject(R.id.listview_put)
	private ListView putListView;
	@ViewInject(R.id.hint)
	private TextView hintView;
	@ViewInject(R.id.btn_getgold)
	private Button getBtn;
	@ViewInject(R.id.btn_putgold)
	private Button putBtn;
	
	
	private GetJiFenAdapter getAdapter;
	private PutJiFenAdapter putAdapter;
	private ArrayList<GetJiFenBean> getList = new ArrayList<GetJiFenBean>();
	private ArrayList<PutJiFenBean> putList = new ArrayList<PutJiFenBean>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去掉标题栏
		setContentView(R.layout.integral_layout);
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancel(1);
		ViewUtils.inject(this);
		loadData(PhoneInfoUtils.getIMEI(getApplicationContext()));
		putListView.setEmptyView(hintView);
	}

	private void loadData(String biaozhifu) {
		getJifen(biaozhifu);
		loadGet(biaozhifu);
		loadPut(biaozhifu);
	}

	/**
	 * 
	 * 方法说明：加载获取的积分详情
	 * 
	 * @param biaozhifu
	 */
	private void loadGet(String biaozhifu) {
		ProtocolService.obtainPoints(biaozhifu, new RequestCallBack<String>() {
			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				try {
					org.json.JSONObject obj = new org.json.JSONObject(
							arg0.result);
					if (obj.getInt("message") == 1) {
						JSONArray array = new JSONArray(obj.getString("0"));
						for (int i = 0; i < array.length(); i++) {
							org.json.JSONObject Obj = array.getJSONObject(i);
							GetJiFenBean bean = new GetJiFenBean();
							bean.setGet_inte_integral("+"+Obj
									.getString("get_inte_integral")+"金币");
							String timeS = Obj.getString("get_inte_time");
							bean.setGet_inte_time(MethodUtils.getTimer(timeS));
							bean.setGet_inte_type(Obj
									.getString("get_inte_type"));
							getList.add(bean);
						}
						getAdapter = new GetJiFenAdapter(getList,
								getApplicationContext());
						setAdapters(getAdapter);
					} else {

					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(HttpException arg0, String arg1) {
				ToastUtils.show(getApplicationContext(),
						R.string.phone_verify_send_fail);
			}
		});
	}

	/**
	 * 
	 * 方法说明：加载消费的积分详情
	 * 
	 * @param biaozhifu
	 */
	private void loadPut(String biaozhifu) {
		ProtocolService.putPoints(biaozhifu, new RequestCallBack<String>() {
			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				try {
					org.json.JSONObject obj = new org.json.JSONObject(
							arg0.result);
					if (obj.getInt("message") == 1) {
						JSONArray array = new JSONArray(obj.getString("0"));
						for (int i = 0; i < array.length(); i++) {
							org.json.JSONObject Obj = array.getJSONObject(i);
							PutJiFenBean bean = new PutJiFenBean();
							bean.setSonsume_inte_integral("-"+Obj
									.getString("sonsume_inte_integral")+"金币");
							String timeS = Obj.getString("sonsume_inte_time");
							bean.setSonsume_inte_time(MethodUtils.getTimer(timeS));
							bean.setSonsume_inte_type(Obj
									.getString("sonsume_inte_type"));
							putList.add(bean);
						}
						putAdapter = new PutJiFenAdapter(putList,
								getApplicationContext());
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(HttpException arg0, String arg1) {
				ToastUtils.show(getApplicationContext(),
						R.string.phone_verify_send_fail);
			}
		});
	}

	/**
	 * 
	 * 方法说明：获取积分数据
	 * 
	 * @param biaozhifu
	 */
	private void getJifen(String biaozhifu) {
		ProtocolService.getJiFen(biaozhifu, new RequestCallBack<String>() {
			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				System.out.println();
				JSON json = JSONObject.parseObject(arg0.result, JSON.class);
				history.setText(json.getHistory_grande());
				current.setText(json.getIntegral_grande());
			}

			@Override
			public void onFailure(HttpException arg0, String arg1) {
				ToastUtils.show(getApplicationContext(),
						R.string.phone_verify_send_fail);
			}
		});

	}

	@OnClick({ R.id.come_back})
	public void back(View v) {
		IntegralActivity.this.finish();
	}
	@OnClick({R.id.btn_putgold , R.id.btn_getgold })
	public void cutList(View v){
		Button btn = (Button) v;
		switch (v.getId()) {
		case R.id.btn_putgold:
			if(btn.isEnabled()){
				btn.setEnabled(false);
				btn = (Button) findViewById(R.id.btn_getgold);
				btn.setEnabled(true);
				setAdapters(putAdapter);
			}
			break;
		case R.id.btn_getgold:
			if(btn.isEnabled()){
				btn.setEnabled(false);
				btn = (Button) findViewById(R.id.btn_putgold);
				btn.setEnabled(true);
				setAdapters(getAdapter);
			}
			
			break;

		}
	}
	
	private void setAdapters(BaseAdapter adapter){
		
		if (adapter != null) {
			if(!putListView.isEnabled()){
				putListView.removeAllViews();
			}
			putListView.setAdapter(adapter);
		}
	}
}
