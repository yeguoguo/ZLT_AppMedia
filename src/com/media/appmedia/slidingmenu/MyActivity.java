package com.media.appmedia.slidingmenu;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.media.appmedia.R;
import com.media.appmedia.activity.AdWebView;
import com.media.appmedia.base.BaseActivity;
import com.media.appmedia.base.BaseApplication;
import com.media.appmedia.base.UserInfo;
import com.media.appmedia.entity.Data_A;
import com.media.appmedia.entity.JSON;
import com.media.appmedia.entity.UserBean;
import com.media.appmedia.exchange.QQActivity;
import com.media.appmedia.network.ProtocolConst;
import com.media.appmedia.network.ProtocolService;
import com.media.appmedia.util.MethodUtils;
import com.media.appmedia.util.NetUtil;
import com.media.appmedia.util.PhoneInfoUtils;
import com.media.appmedia.util.ToastUtils;

/**
 * 
 * <pre>
 * 业务名:
 * 功能说明: 侧滑菜单里的我的信息
 * 编写日期:	2014-10-23
 * 作者:	 季佳满
 * 
 * 历史记录
 * 1、修改日期：
 *    修改人：
 *    修改内容：
 * </pre>
 */
public class MyActivity extends BaseActivity {

	private Context mContext;
	private UserInfo userInfo;
	private final int DATA_NULL = 2;// 无数据标志
	private final int NET_WORK_ERROR = 3;// 网络异常标志
	private final int DATACONTENT = 4;// 内容获得标志
	private List<Data_A> list = new ArrayList<Data_A>();
//	@ViewInject(R.id.changeImage)
//	private ImageButton imageButton;
	@ViewInject(R.id.user_name)
	private TextView myName;
	@ViewInject(R.id.user_grade)
	private TextView mGrade;
	@ViewInject(R.id.user_next_grade)
	private TextView mNextGrade;
	@ViewInject(R.id.user_integ)
	private TextView mInteg;
	@ViewInject(R.id.user_gold)
	private TextView mGold;
	@ViewInject(R.id.user_next_gold)
	private TextView mNextGold;
	private String[] grade = new String[] { "平民", "衙役", "县令", "知府", "总督", "巡抚",
			"丞相", "皇帝", "已达最高" };
	private String[] integ = new String[] { "1000", "3000", "10000", "50000",
			"100000", "500000", "1000000", "已达最高" };
	private String[] award = new String[] { "0%", "5%", "10%", "15%", "20%",
			"25%", "30%", "35%", };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.my_layout);
		ViewUtils.inject(this);
		String phone = MethodUtils.getUserInfo(getApplicationContext());
		String mobId = PhoneInfoUtils.getIMEI(getApplicationContext());
		getVeriMyInfo(phone, mobId);

	}

	/**
	 * 
	 * 方法说明：设置当前 i 处的等级以及下一级范围
	 * 
	 * @param i
	 */
	private void setInfo(int i) {
		mGrade.setText(grade[i]);
		mNextGrade.setText(grade[i + 1]);
		mGold.setText("(" + userInfo.getInteg() + "金币)");
		mInteg.setText("享受" + award[i] + "的金币加成");
		mNextGold.setText("(" + integ[i] + "金币)");
	}

	private int getUserGrade() {
		int type = 0;
		String integ;
		float integInt;
		if (userInfo != null && userInfo.getUserName() != null) {
			integ = userInfo.getInteg();
			integInt = Float.parseFloat(integ);
			if (integInt > 0 && integInt < 1000) {
				type = 0;
			} else if (integInt < 3000) {
				type = 1;
			} else if (integInt < 10000) {
				type = 2;
			} else if (integInt < 50000) {
				type = 3;
			} else if (integInt < 100000) {
				type = 4;
			} else if (integInt < 500000) {
				type = 5;
			} else if (integInt < 1000000) {
				type = 6;
			} else if (integInt > 1000000) {
				type = 7;
			}

		}
		return type;

	}

	public Handler mHandler = new Handler() {
		@SuppressWarnings("unchecked")
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case DATA_NULL:
				Toast.makeText(mContext, "数据木有哦！！！", Toast.LENGTH_SHORT).show();
				break;
			case NET_WORK_ERROR:
				Toast.makeText(mContext, "网络木有哦！！！", Toast.LENGTH_SHORT).show();
				break;
			case DATACONTENT:
				list = (List<Data_A>) msg.obj;
				break;
			}
		};
	};

	// private void initData(){
	// new Thread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // TODO Auto-generated method stub
	// if(NetUtil.isNetConn(mContext)){
	// String jsonstr =
	// NetUtil.getStringFromUrlByHttpClient(ProtocolConst.LEIJI_URL);
	// Log.i("Tag", "--------------------------"+jsonstr);
	// if(jsonstr!=null){
	// List<Data_A> mmlist = parseJSON(jsonstr).getData();
	// Log.i("Tag", "--------------------------"+mmlist.toString());
	// Message msg = mHandler.obtainMessage(DATACONTENT, mmlist);
	// mHandler.sendMessage(msg);
	// }else{
	// mHandler.sendEmptyMessage(DATA_NULL);
	// }
	// }else{
	// mHandler.sendEmptyMessage(NET_WORK_ERROR);
	// }
	// }
	// }).start();
	//
	// }

	/**
	 * 
	 * 方法说明：
	 * 
	 * @param phoneNumber
	 *            手机号码
	 * @param type
	 *            手机ID
	 * @param timer
	 */
	protected void getVeriMyInfo(String phoneNumber, String type) {
		ProtocolService.getVerifyMy(phoneNumber, type,
				new RequestCallBack<String>() {
					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						UserBean userBean = JSONObject.parseObject(arg0.result,
								UserBean.class);
						if (userBean != null && userBean.getMessage() == 1) {
							UserInfo info = BaseApplication.getApplication()
									.getUserInfo();
							info.setUserName(userBean.getRole());
							info.setInteg(userBean.getHistory());
							userInfo = BaseApplication.getApplication()
									.getUserInfo();
							if (userInfo != null
									&& userInfo.getUserName() != null) {
								myName.setText(userInfo.getUserName().trim());
							}
							if(userBean.getShare().equals(1+"")){
								AlertDialog.Builder alertbBuilder = new AlertDialog.Builder(
										MyActivity.this);
								alertbBuilder
										.setTitle("提示")
										.setMessage("恭喜您升官了！\n分享立即获得20金币")
										.setPositiveButton(
												"分享到朋友圈",
												new DialogInterface.OnClickListener() {
													@Override
													public void onClick(
															DialogInterface dialog,
															int which) {
														dialog.cancel();
														initSocialSDK("老子今天官升一级，终于可以耀武扬威，作威作福，横行霸道，任性的活了。还有每天拿的人民币也多了，现在挥金如土，穷奢极欲，一掷千金，随意的花了。"
																,1+"");
													}
												}).create();
								alertbBuilder.show();
							}
							// 设置当前用户所属等级和积分范围
							setInfo(getUserGrade());
						}
					}

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						ToastUtils.show(getApplicationContext(),
								R.string.phone_verify_send_fail);
					}
				});
	}

	@OnClick({ R.id.come_back})
	public void back(View v) {
		MyActivity.this.finish();
	}
}
